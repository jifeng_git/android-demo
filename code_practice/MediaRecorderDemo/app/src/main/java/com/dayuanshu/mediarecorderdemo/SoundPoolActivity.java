package com.dayuanshu.mediarecorderdemo;

import android.media.SoundPool;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class SoundPoolActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {
    static class Sound{
        String name;
        int soundId;

        public Sound(String name, int soundId) {
            this.name = name;
            this.soundId = soundId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getSoundId() {
            return soundId;
        }

        public void setSoundId(int soundId) {
            this.soundId = soundId;
        }
    }
    private List<Sound> data;
    private SoundPool soundPool;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soundpool);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        data=new ArrayList<>();
        soundPool=new SoundPool.Builder().setMaxStreams(3).build();

        data.add(new Sound("a1",soundPool.load(this,R.raw.a1,1)));
        data.add(new Sound("a2",soundPool.load(this,R.raw.a2,1)));
        data.add(new Sound("a3",soundPool.load(this,R.raw.a3,1)));
        MyAdapter myAdapter = new MyAdapter(data, recyclerView,this);
        myAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(myAdapter);

    }

    /**
     * 点击事件
     * @param position
     */
    @Override
    public void onItemClick(int position) {
        Sound sound = data.get(position);
        // 播放
        soundPool.play(sound.getSoundId(),1.0f,0.5f,1,0,1.0f);
        for (Sound datum : data) {
            soundPool.unload(datum.getSoundId());
        }
        soundPool.release();
    }
}
