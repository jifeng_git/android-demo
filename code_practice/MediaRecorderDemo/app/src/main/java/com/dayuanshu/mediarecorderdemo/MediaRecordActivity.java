package com.dayuanshu.mediarecorderdemo;

import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;

public class MediaRecordActivity extends AppCompatActivity implements View.OnClickListener {
    private TextureView textureView;
    private Button btnOpt;
    private MediaRecorder mediaRecorder;
    private Camera camera;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        textureView = findViewById(R.id.text_view);
        btnOpt = findViewById(R.id.btn_opt);
        btnOpt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        CharSequence text = btnOpt.getText();
        if(TextUtils.equals(text,"开始")){
            btnOpt.setText("结束");
            // 修正预览图像颠倒的状态
            camera=Camera.open();
            camera.setDisplayOrientation(90);
            camera.unlock();
            mediaRecorder = new MediaRecorder();
            // 修正预览颠倒
            mediaRecorder.setCamera(camera);
            // 设置音频,麦克风
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            // 设备视频源,摄像头
            mediaRecorder.setVideoSource(MediaRecorder.AudioSource.CAMCORDER);
            // 指定视频文件格式
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            // 音频
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            // 视频
            mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            // 修正录制出来的文件是颠倒的状态
            mediaRecorder.setOrientationHint(90);
            // 设置视频输出文件
            mediaRecorder.setOutputFile(new File(getExternalFilesDir(""),"a.mp4").getAbsolutePath());
            // 设置视频的大小
            mediaRecorder.setVideoSize(640,480);
            // 摄像头预览的画布
            mediaRecorder.setPreviewDisplay(new Surface(textureView.getSurfaceTexture()));
            // 进入准备阶段
            try {
                mediaRecorder.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mediaRecorder.start();
        }else {
            btnOpt.setText("开始");
            mediaRecorder.stop();
            mediaRecorder.release();
            // 释放
            camera.stopPreview();
            camera.release();
        }
    }
}
