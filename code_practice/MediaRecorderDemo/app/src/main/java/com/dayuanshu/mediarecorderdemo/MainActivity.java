package com.dayuanshu.mediarecorderdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 申请权限
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO},
                100);
    }

    /**
     * 录制视频
     * @param view
     */
    public void record(View view) {
        startActivity(new Intent(this,MediaRecordActivity.class));
    }

    /**
     * 播放视频
     * @param view
     */
    public void playVideo(View view) {
//        startActivity(new Intent(this,VideoActivity.class));
        startActivity(new Intent(this,VideoViewActivity.class));
    }

    /**
     * 播放音效
     * @param view
     */
    public void playAudio(View view) {
        startActivity(new Intent(this,SoundPoolActivity.class));
    }
}