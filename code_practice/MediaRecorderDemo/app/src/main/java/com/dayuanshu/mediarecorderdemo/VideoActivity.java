package com.dayuanshu.mediarecorderdemo;

import android.hardware.Camera;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;

public class VideoActivity extends AppCompatActivity{
    private TextureView textureView;
    private Button btnOpt2;
    private MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        textureView = findViewById(R.id.texture_view);
        btnOpt2 = findViewById(R.id.btn_opt2);
//        btnOpt2.setOnClickListener(this);
        btnOpt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence text = btnOpt2.getText();
                if(TextUtils.equals(text,"开始")) {
                    btnOpt2.setText("结束");
                    // 1.初始状态
                    mediaPlayer = new MediaPlayer();
                    // 2.初始化完成的状态
                    // 4.设置准备监听
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            // 开始播放
                            mediaPlayer.start();
                        }
                    });
                    // 播放是否完成
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            btnOpt2.setText("开始");
                            mediaPlayer.release();
                        }
                    });
                    try {
                        // 指定视频源
                        mediaPlayer.setDataSource(new File(getExternalFilesDir("C:\\Users\\admin\\Videos\\Captures\\"),"a.mp4").getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // 5.设置画布展示来展示图像
                    mediaPlayer.setSurface(new Surface(textureView.getSurfaceTexture()));
                    // 3.异步准备
                    mediaPlayer.prepareAsync();
                    // 6.进度条的显示

                }else {
                    btnOpt2.setText("开始");
                    mediaPlayer.stop();
                    // 释放掉
                    mediaPlayer.release();
                }
            }
        });
    }

    /**
     * 按钮的点击事件
     * @param v
     */
    /*@Override
    public void onClick(View v) {

    }*/
}
