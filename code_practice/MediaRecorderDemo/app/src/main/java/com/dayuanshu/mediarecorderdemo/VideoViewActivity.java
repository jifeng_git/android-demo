package com.dayuanshu.mediarecorderdemo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

public class VideoViewActivity extends AppCompatActivity implements View.OnClickListener {

    private VideoView videoView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoview);
        videoView = findViewById(R.id.videoView);
        MediaController mediaController = new MediaController(this);
        mediaController.setPrevNextListeners(this,this);
        videoView.setMediaController(mediaController);
        videoView.setVideoPath(new File(getExternalFilesDir("C:\\Users\\admin\\Videos\\Captures\\"),"a.mp4").getAbsolutePath());
        videoView.start();
    }

    @Override
    public void onClick(View v) {
        Log.i("VideoView","===========监听上一个下一个视频播放哪一个");
    }
}
