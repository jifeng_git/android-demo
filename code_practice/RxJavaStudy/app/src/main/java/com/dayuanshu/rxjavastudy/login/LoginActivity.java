package com.dayuanshu.rxjavastudy.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.dayuanshu.rxjavastudy.R;
import com.dayuanshu.rxjavastudy.login.bean.SuccessBean;
import com.dayuanshu.rxjavastudy.login.core.CustomObserver;
import com.dayuanshu.rxjavastudy.login.core.LoginEngine;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LoginEngine.login("JiFeng","123456")
                // 返回起点,起点中包含总bean
                .subscribe(new CustomObserver() {
                    @Override
                    public void success(SuccessBean successBean) {
                        Log.d("JiFeng","成功bean的 详情 success:SuccessBean:"+successBean.toString());
                    }

                    @Override
                    public void error(String message) {
                        Log.d("JiFeng","String类型 error: message:"+message);
                    }
                });
    }
}