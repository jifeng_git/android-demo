package com.dayuanshu.rxjavastudy;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {
    // 打印Logcat日志的变迁
    private final String TAG = MainActivity.class.getSimpleName( );

    // 网络图片的连接地址
    private final static String PATH = "http://pic1.win4000.com/wallpaper/c/53cdd1f7c1f21.jpg";

    // 弹出加载框(正在加载中...)
    private ProgressDialog progressDialog;
    // imageview控件
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView=findViewById(R.id.image_view);
    }

    /**
     * 图片显示加载功能
     * @param view
     */
    public void showImageAction(View view) {
        /**
         * TODD RX思维
         * 起点 和 终点
         * RxJava RxJs RxXXX RX系列框架 为什么把所有函数都称为操作符 因为我们函数要去操作 从起点流向终点
         */
        // 起点
        // 第二步
        Observable.just(PATH)
                // 第三步
                // PATH-->BitMap
                .map(new Function<String, Bitmap>() {
                    @Override
                    public Bitmap apply(String path) throws Exception {
                        try {
                            // 服务器的请求操作
                            URL url=new URL(path);
                            HttpURLConnection httpURLConnection= (HttpURLConnection) url.openConnection();
                            // 设置请求连接时长
                            httpURLConnection.setConnectTimeout(5000);
                            // 才开始 Request 拿到服务器的响应 200成功 404有问题
                            int responseCode = httpURLConnection.getResponseCode();
                            if(responseCode==HttpURLConnection.HTTP_OK){
                                InputStream inputStream = httpURLConnection.getInputStream();
                                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                                return bitmap;
                            }
                        }catch (Exception e){
                            e.getStackTrace();
                        }
                        return null;
                    }
                })
                // 需求:002加水印
                .map(new Function<Bitmap, Bitmap>() {
                    @Override
                    public Bitmap apply(Bitmap bitmap) throws Exception {
                        Paint paint=new Paint();
                        paint.setColor(Color.RED);
                        paint.setTextSize(80);
                        Bitmap shuiyingBitmap = drawTextToBitmap(bitmap, "大源数", paint, 88, 88);
                        return shuiyingBitmap;
                    }
                })
                //需求:003日志记录需求
                .map(new Function<Bitmap, Bitmap>() {
                    @Override
                    public Bitmap apply(Bitmap bitmap) throws Exception {
                        Log.e(TAG,"什么时候下载了图片 apply"+System.currentTimeMillis());
                        return null;
                    }
                })
                // 给上面分配异步线程(图片下载操作...)
                .subscribeOn(Schedulers.io())
                // 给终点分配安卓主线程
                .observeOn(AndroidSchedulers.mainThread())
                // 导火索,点燃了,开始执行
                // 关联:观察者设计模式 关联起点和终点 == 订阅
                .safeSubscribe(new Observer<Bitmap>() {
                    // 第一步
                    // 订阅成功
                    @Override
                    public void onSubscribe(Disposable d) {
                        // 显示加载框
                        progressDialog=new ProgressDialog(MainActivity.this);
                        progressDialog.setTitle("RXJava 大源数 run 正在加载中...");
                        progressDialog.show();
                    }
                    // 第四步:显示图片,水印bitMap
                    // 上一层给我的响应
                    @Override
                    public void onNext(Bitmap bitmap) {
                        // 显示到控件上
                        imageView.setImageBitmap(bitmap);

                    }
                    // 链条思维发生了异常
                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG,"发生了异常"+e.getMessage());
                    }
                    // 第五步
                    // 整个链条全部结束
                    @Override
                    public void onComplete() {
                        // 隐藏加载框
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                    }
                });
    }

    /**
     * 图片上绘制文字,加水印
     * @param bitmap
     * @param text
     * @param paint 画笔
     * @param paddingLeft
     * @param paddingTop
     * @return
     */
    private final Bitmap drawTextToBitmap(Bitmap bitmap, String text, Paint paint,int paddingLeft,int paddingTop){
        Bitmap.Config bitmapConfig=bitmap.getConfig();
        // 获取更清晰的图像采样
        paint.setDither(true);
        // 过滤一些
        paint.setFilterBitmap(true);
        if(bitmapConfig==null){
            bitmapConfig=Bitmap.Config.ARGB_8888;
        }
        bitmap=bitmap.copy(bitmapConfig,true);
        Canvas canvas=new Canvas(bitmap);

        canvas.drawText(text,paddingLeft,paddingTop,paint);
        return bitmap;
    }

    // 常用操作符
    public void actionOperate(View view) {
        String[] strs={"AAA","BBB","CCC"};
       /* for (String str : strs) {

        }*/
        // 起点
        Observable.fromArray(strs)
                // 订阅起点和终点
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        Log.d(TAG,"accept"+s);
                    }
                });
    }
}