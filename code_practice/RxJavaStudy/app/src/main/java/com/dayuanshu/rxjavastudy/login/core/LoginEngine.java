package com.dayuanshu.rxjavastudy.login.core;

import com.dayuanshu.rxjavastudy.login.bean.ResponseResult;
import com.dayuanshu.rxjavastudy.login.bean.SuccessBean;

import io.reactivex.Observable;

public class LoginEngine {



    // 返回 起点
    public static Observable<ResponseResult> login(String name,String pwd){
        // 最终返回总部bean
        ResponseResult responseResult=new ResponseResult();
        if("JiFeng".equals(name)&&"123456".equals(pwd)){// 登录成功
            SuccessBean successBean = new SuccessBean();
            successBean.setId(1001);
            successBean.setName("JiFeng登录成功");
            /**
             * data:{xxxx 登录成功的bean  xxxx successbean 成功bean}
             * code:200
             * message:"登录成功"
             */
            responseResult.setData(successBean);
            responseResult.setCode(200);
            responseResult.setMessage("登录成功");
        }else {// 登录失败
            /**
             * data:null
             * code:404
             * message:"登录错误"
             */
            responseResult.setData(null);
            responseResult.setCode(404);
            responseResult.setMessage("登录错误");
        }
        // 起点
        return Observable.just(responseResult);
    }
 }
