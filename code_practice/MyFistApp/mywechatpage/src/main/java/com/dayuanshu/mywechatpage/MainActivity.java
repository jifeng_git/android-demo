package com.dayuanshu.mywechatpage;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    ViewPager2 viewPager2;
    private LinearLayout llWechat,llContacts,llFind,llMine;
    private ImageView ivWechat,ivContacts,ivFind,ivMine,ivCurrent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initPager();
        initTabView();
    }
    // 专门处理UI
    private void initTabView() {
        llWechat=findViewById(R.id.id_tab_wechat);
        // 监听
        llWechat.setOnClickListener(this);
        llContacts=findViewById(R.id.id_tab_contact);
        // 监听
        llContacts.setOnClickListener(this);
        llFind=findViewById(R.id.id_tab_find);
        // 监听
        llFind.setOnClickListener(this);
        llMine=findViewById(R.id.id_tab_mine);
        // 监听
        llMine.setOnClickListener(this);
        // 图标
        ivWechat=findViewById(R.id.id_iv_wechat);
        ivContacts=findViewById(R.id.id_iv_contact);
        ivFind=findViewById(R.id.id_iv_find);
        ivMine=findViewById(R.id.id_iv_mine);

        // 默认选中第一个
        ivWechat.setSelected(true);
        // 当前按钮
        ivCurrent = ivWechat;
    }


    private void initPager() {
        viewPager2=findViewById(R.id.id_viewpager2);
        ArrayList<Fragment> fragments=new ArrayList<>();
        fragments.add(BlankFragment.newInstance("微信聊天"));
        fragments.add(BlankFragment.newInstance("通讯录"));
        fragments.add(BlankFragment.newInstance("发现"));
        fragments.add(BlankFragment.newInstance("我"));
        // jetpack
        MyFragmentPagerAdapter pagerAdapter=new MyFragmentPagerAdapter(getSupportFragmentManager(),getLifecycle(),fragments);
        viewPager2.setAdapter(pagerAdapter);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                // 滑动绑定
                changeTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });
    }

    private void changeTab(int position) {
        // 复位
        ivCurrent.setSelected(false);
        switch (position) {
            case R.id.id_tab_wechat:
                viewPager2.setCurrentItem(0);
            case 0:
                ivWechat.setSelected(true);
                ivCurrent=ivWechat;
                break;
            case R.id.id_tab_contact:
                viewPager2.setCurrentItem(1);
            case 1:
                ivContacts.setSelected(true);
                ivCurrent=ivContacts;
                break;
            case R.id.id_tab_find:
                viewPager2.setCurrentItem(2);
            case 2:
                ivFind.setSelected(true);
                ivCurrent=ivFind;
                break;
            case R.id.id_tab_mine:
                viewPager2.setCurrentItem(3);
            case 3:
                ivMine.setSelected(true);
                ivCurrent=ivMine;
                break;
        }
    }

    @Override
    public void onClick(View v) {
        changeTab(v.getId());
    }
}