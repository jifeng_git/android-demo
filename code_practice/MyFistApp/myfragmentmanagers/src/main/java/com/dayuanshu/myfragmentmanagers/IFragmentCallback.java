package com.dayuanshu.myfragmentmanagers;

/**
 * 发送消息给Activity
 */
public interface IFragmentCallback {
    /**
     * 发送消息给Activity
     * @param message 发送的消息
     */
    void sendMsgToActivity(String message);

    /**
     * 获取到发送的消息
     * @param msg 消息
     * @return
     */
    String getMsgFromActivity(String msg);
}
