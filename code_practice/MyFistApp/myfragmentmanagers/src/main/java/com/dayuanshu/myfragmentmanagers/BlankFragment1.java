package com.dayuanshu.myfragmentmanagers;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class BlankFragment1 extends Fragment {
    private View rootView;

    public BlankFragment1() {
    }

    /**
     * 生命周期的创建
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=this.getArguments();
        String message = bundle.getString("message");
        Log.e("dys","onCreate:获取的activity值是:"+message);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        if(rootView==null){
            rootView=inflater.inflate(R.layout.fragment_blank1, container, false);
        }
        Button btn3=rootView.findViewById(R.id.btn3);
        // 点击事件
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragmentCallback.sendMsgToActivity("你好,我来自Fragment");
                String msg = fragmentCallback.getMsgFromActivity("null");
                Toast.makeText(BlankFragment1.this.getContext(),msg,Toast.LENGTH_SHORT).show();
            }
        });
        Log.e("dys","onCreateView:");
        return rootView;
    }

    private IFragmentCallback fragmentCallback;
    public void setFragmentCallback(IFragmentCallback callback){
        fragmentCallback=callback;
        Log.e("dys","setFragmentCallback:");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("dys","onActivityCreated:");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("dys","onStart:");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("dys","onResume:");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("dys","onPause:");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("dys","onStop:");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("dys","onDestroyView:");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("dys","onDestroy:");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("dys","onDetach:");
    }
}