package com.dayuanshu.myfragmentmanagers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Binder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn1 = findViewById(R.id.btn);
        btn1.setOnClickListener(this);
        Button btn2 = findViewById(R.id.btn2);
        btn2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn:
                Bundle bundle = new Bundle();
                bundle.putString("message","大源数");
                BlankFragment1 bf = new BlankFragment1();
                //replaceFragment(new BlankFragment1());
                bf.setArguments(bundle);
                replaceFragment(bf);
                bf.setFragmentCallback(new IFragmentCallback() {
                    @Override
                    public void sendMsgToActivity(String message) {
                        Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public String getMsgFromActivity(String msg) {
                        return "你好,我来自MainActivity";
                    }
                });
                break;
            case R.id.btn2:
                replaceFragment(new ItemFragment());
                break;
        }
    }
    // 动态切换Fragment
    private void replaceFragment(Fragment fragment) {
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.framelayout,fragment);
        // 把fragment添加到同一个栈中,生命周期有何不一样
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}