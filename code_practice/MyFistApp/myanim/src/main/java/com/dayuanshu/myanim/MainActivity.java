package com.dayuanshu.myanim;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {
    private boolean flag=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RelativeLayout relativeLayout=findViewById(R.id.lt_root);

        AnimationDrawable animBackground = (AnimationDrawable) relativeLayout.getBackground();

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag){
                    animBackground.start();
                    flag=false;
                }else {
                    animBackground.stop();
                    flag=true;
                }
            }
        });
    }
}