package com.dayuanshu.myfragmentandviewpager2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewPager2 viewPage2 = findViewById(R.id.viewPage2);
        ViewPagerAdapter viewPagerAdapter=new ViewPagerAdapter();
        viewPage2.setAdapter(viewPagerAdapter);
    }
}