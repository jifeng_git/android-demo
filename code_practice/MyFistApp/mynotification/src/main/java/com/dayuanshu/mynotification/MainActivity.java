package com.dayuanshu.mynotification;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private NotificationManager manager;
    private Notification notification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel("dys", "测试通知", NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel);
        }
        Intent intent = new Intent(this, NotificationActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        notification = new NotificationCompat.Builder(this,"dys")
                .setContentTitle("官方通知")
                .setContentText("世界那么大,想去闯闯吗？")
                // 不带颜色的图片
                .setSmallIcon(R.drawable.ic_baseline_person_24)
                // 设置大图标
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.city))
                // 设置小图标的颜色
                .setColor(Color.parseColor("#ff0000"))
                // 点击通知跳转
                .setContentIntent(pendingIntent)
                // 点击取消通知
                .setAutoCancel(true)
                .build();
    }

    // 发出通知
    public void sendNotification(View view) {
        manager.notify(1,notification);
    }

    // 取消通知
    public void cancelNotification(View view) {
        manager.cancel(1);
    }
}