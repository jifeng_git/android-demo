package com.dayuanshumybutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    public static final String TAG="DYS";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = findViewById(R.id.btn);
        // 点击事件
        /*btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG,"onclick");
            }
        });*/
        // 长按事件return true:先去调用onTouch事件,执行完成后,执行onLongClick
        btn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.e(TAG,"onLongClick:");
                return false;
            }
        });
        // 触摸事件 return true:事件被onTouch消费了;
        btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.e(TAG,"onTouch:"+motionEvent.getAction());
                return false;
            }
        });
    }
    // 替代onClick的方法
    public void dysClick(View view) {
        Log.e(TAG,"onclick");
    }
}