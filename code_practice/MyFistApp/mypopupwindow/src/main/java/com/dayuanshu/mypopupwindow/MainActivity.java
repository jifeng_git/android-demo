package com.dayuanshu.mypopupwindow;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    // 点击弹出popupWindow框
    public void popupWindowClick(View view) {
        View popupView = getLayoutInflater().inflate(R.layout.popup_view, null);
        Button btn1 = popupView.findViewById(R.id.btn1);
        Button btn2 = popupView.findViewById(R.id.btn2);
        PopupWindow popupWindow =
                new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT,true);
//        popupWindow.showAsDropDown(view,view.getWidth(),-view.getHeight());
        popupView.setBackgroundDrawable(getResources().getDrawable(R.drawable.stars));
        popupWindow.showAsDropDown(view);
        // 按钮1点击事件
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("dys","你是住在上海吗?");
            }
        });
        // 按钮2点击事件
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("dys","你是住在北京吗?");
                // 点击后关闭弹窗
                popupWindow.dismiss();
            }
        });
    }
}