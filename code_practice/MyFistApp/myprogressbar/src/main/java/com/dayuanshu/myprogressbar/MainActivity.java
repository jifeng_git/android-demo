package com.dayuanshu.myprogressbar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {
    private ProgressBar pb;
    private ProgressBar pb2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         pb = findViewById(R.id.pb);
         pb2 = findViewById(R.id.pb2);
    }
    // 按钮点击事件控制进度条显示跟隐藏
    public void pdOnclick(View view) {
        // 网络数据请求结果
        // 进度条隐藏
        if(pb.getVisibility()== View.GONE){
            // 显示
            pb.setVisibility(View.VISIBLE);
        }else {
            pb.setVisibility(View.GONE);
        }
    }
    // 点击下载文件
    public void downloadClick(View view) {
        // 获取当前进度条的值
        int progress = pb2.getProgress();
        progress+=10;
        pb2.setProgress(progress);
    }
}