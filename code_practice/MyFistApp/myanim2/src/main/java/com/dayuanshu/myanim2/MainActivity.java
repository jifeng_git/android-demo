package com.dayuanshu.myanim2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = findViewById(R.id.iv);
        // 点击事件
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 通过加载xml动画设置文件来创建一个Animation对象
                /*Animation animation =
                        AnimationUtils.loadAnimation(MainActivity.this, R.anim.alpha);

                imageView.startAnimation(animation);*/
                // 旋转
                /*Animation animation =
                        AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate);
                imageView.startAnimation(animation);*/
                // 缩放
               /* Animation animation =
                        AnimationUtils.loadAnimation(MainActivity.this, R.anim.scale);
                imageView.startAnimation(animation);*/
                Animation animation =
                        AnimationUtils.loadAnimation(MainActivity.this, R.anim.translate);
                imageView.startAnimation(animation);
            }
        });
    }
}