package com.dayuanshu.myactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

public class MainActivity4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        // Java代码注册刚刚的接收者即可
        // 第二步,在onCreate注册广播(订阅)
        // 动态使用Java代码注册一个广播接收者
        UpdateIpSelectCity updateIpSelectCity=new UpdateIpSelectCity();
        IntentFilter filter=new IntentFilter();
        filter.addAction(ActionUtils.ACTION_EQUES_UPDATE_IP);
        registerReceiver(updateIpSelectCity,filter);
    }
    // 第二步:发送给接收者
    // 静态发送广播给接收者
    public void sendAction2(View view) {
        Intent intent = new Intent();
        intent.setAction(ActionUtils.ACTION_FLAG);
        sendBroadcast(intent);
    }

    // 第三步:
    // 发送给动态注册的接收者
    public void sendAction1(View view) {
        Intent intent = new Intent();
        intent.setAction(ActionUtils.ACTION_EQUES_UPDATE_IP);
        sendBroadcast(intent);
    }
}