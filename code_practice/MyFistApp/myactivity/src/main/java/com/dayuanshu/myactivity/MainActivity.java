package com.dayuanshu.myactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private final String TAG=MainActivity.class.getCanonicalName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e(TAG,TAG+"------>onCreate()");
    }

    /**
     * 重新启动
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG,TAG+"------>onRestart()");
    }

    /**
     * 启动
     */
    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG,TAG+"------>onStart()");
    }

    /**
     * 画面渲染已经完成
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,TAG+"------>onResume()");
    }

    /**
     * 画面被暂停,卡片不可见
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG,TAG+"------>onPause()");
    }

    /**
     * 被停止
     */
    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG,TAG+"------>onStop()");
    }

    /**
     * 画面被销毁
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG,TAG+"------>onDestroy()");
    }

    // 点击跳转
    public void startActivity2(View view) {
        startActivity(new Intent(this,MainActivity2.class));
    }

}