package com.dayuanshu.myactivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

// 广播接收者
public class CustomReceiver extends BroadcastReceiver {
    private final String TAG=CustomReceiver.class.getCanonicalName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG,"CustomReceiver onReceive广播接收者");
    }
}
