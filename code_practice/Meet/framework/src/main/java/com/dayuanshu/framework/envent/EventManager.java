package com.dayuanshu.framework.envent;

import org.greenrobot.eventbus.EventBus;

/**
 * EventBus调度类
 */
public class EventManager {

    // 测试
    public static final int FLAG_TEST=1002;
    // 更新好友列表
    public static final int FLAG_UPDATE_FRIEND_LIST=1000;
    // 发送文本数据
    public static final int FLAG_SEND_TEXT=1001;

    /**
     * 注册
     * @param subscriber
     */
    public static void register(Object subscriber){
        EventBus.getDefault().register(subscriber);
    }

    /**
     * 反注册
     * @param subscriber
     */
    public static void unregister(Object subscriber){
        EventBus.getDefault().unregister(subscriber);
    }

    /**
     * 发送事件
     * @param type
     */
    public static void post(int type){
        EventBus.getDefault().post(new MessageEvent(type));
    }
    public static void post(MessageEvent event){
        EventBus.getDefault().post(event);
    }
}
