package com.dayuanshu.framework.envent;

/**
 * 事件类
 */
public class MessageEvent {

    private int type;

    // 文本消息
    private String text;
    private String userId;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public MessageEvent(int type){
        this.type= type;
    }

    public int getType() {
        return type;
    }

    @Override
    public String toString() {
        return "MessageEvent{" +
                "type=" + type +
                ", text='" + text + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
