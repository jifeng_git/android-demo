package com.dayuanshu.framework.manager;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;

import com.dayuanshu.framework.utils.LogUtils;

import java.io.IOException;

/**
 * 媒体播放
 */
public class MediaPlayerManager {

    private  MediaPlayer mMedialPlayer;

    public MediaPlayerManager(){
        mMedialPlayer=new MediaPlayer();
    }
    /**
     * 播放
     */
    public static final int  MEDIA_STATUS_PLAY = 0;
    /**
     * 暂停
     */
    public static final int  MEDIA_STATUS_PAUSE = 1;
    /**
     * 停止
     */
    public static final int  MEDIA_STATUS_STOP = 2;
    /**
     * 当前播放状态
     */
    public int  MEDIA_STATUS = MEDIA_STATUS_STOP;

    /**
     * 在指定的时间过后进行发送
     */
    private static final int H_PROGRESS = 1000;

    private OnMusicProgressListener musicProgressListener;

    /**
     * 计算歌曲的进度:
     * 1.开始播放的时候就开始循环计算时长
     * 2.将进度计算结果对外抛出
     */
    private Handler mHandler=new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what){
                case H_PROGRESS:
                    if(musicProgressListener!=null){
                        // 获取到当前时长
                        int currentPosition=getCurrentPosition();
                        // 获取进度百分比
                        int pos=(int)(((float)currentPosition/(float) getDuration())*100);
                        musicProgressListener.onProgress(currentPosition,pos);
                        // 延迟一秒
                        mHandler.sendEmptyMessageDelayed(H_PROGRESS,1000);
                    }
                    break;
            }
            return false;
        }
    });

    /**
     * 是否在播放 true:是,false:否
     * @return
     */
    public boolean isPlaying(){
        return mMedialPlayer.isPlaying();
    }

    /**
     * 开始播放
     * @RequiresApi(api=Build.VERSION_CODES.N)
     * @param fileDescriptor
     */
    public void startPlay(AssetFileDescriptor fileDescriptor){
        try {
            mMedialPlayer.reset();
            // 设置资源
            mMedialPlayer.setDataSource(fileDescriptor.getFileDescriptor(),
                    fileDescriptor.getStartOffset(),fileDescriptor.getLength());
            // 装载:同步准备播放器进行播放
            mMedialPlayer.prepare();
            // 开始播放
            mMedialPlayer.start();
            // 将状态设置为播放
            MEDIA_STATUS=MEDIA_STATUS_PLAY;
            mHandler.sendEmptyMessage(H_PROGRESS);
        } catch (IOException e) {
            LogUtils.e(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * 播放本地音频
     * @param path
     */
    public void startPlay(String path){
        try {
            // 重置为未初始化状态
            mMedialPlayer.reset();
            // 设置播放资源
            mMedialPlayer.setDataSource(path);
            // 装载:同步准备播放器进行播放
            mMedialPlayer.prepare();
            // 开始播放or恢复播放
            mMedialPlayer.start();
            // 将状态设置为播放
            MEDIA_STATUS=MEDIA_STATUS_PLAY;
            // 发送仅包含 what 值的消息
            mHandler.sendEmptyMessage(H_PROGRESS);
        } catch (IOException e) {
            LogUtils.e(e.toString());
            e.printStackTrace();
        }
    }

    /**
     * 暂停播放
     */
    public void pausePlay(){
        // 在播放
        if(isPlaying()){
            // 暂停播放
            mMedialPlayer.pause();
            // 将状态更改为暂停
            MEDIA_STATUS=MEDIA_STATUS_PAUSE;
            // 删除消息队列中代码为“what”的任何待处理消息
            mHandler.removeMessages(H_PROGRESS);
        }
    }

    /**
     * 继续播放
     */
    public void continuePlay(){
        // 开始播放
        mMedialPlayer.start();
        // 将播放状态更改为继续播放
        MEDIA_STATUS=MEDIA_STATUS_PLAY;
        mHandler.sendEmptyMessage(H_PROGRESS);
    }

    /**
     * 停止播放
     */
    public void stopPlay(){
        // 停止播放
        mMedialPlayer.stop();
        // 将播放状态更改为停止播放
        MEDIA_STATUS=MEDIA_STATUS_STOP;
        mHandler.removeMessages(H_PROGRESS);
    }

    /**
     * 获取当前位置
     * @return
     */
    public int getCurrentPosition(){
        return mMedialPlayer.getCurrentPosition();
    }

    /**
     * 获取总时长
     * @return
     */
    public int getDuration(){
        return mMedialPlayer.getDuration();
    }

    /**
     * 是否循环播放
     * @param isLooping
     */
    public void setLooping(boolean isLooping){
        mMedialPlayer.setLooping(isLooping);
    }

    /**
     * 跳转位置:时间段
     * @param ms
     */
    public void seekTo(int ms){
        mMedialPlayer.seekTo(ms);
    }

    /**
     * 播放结束
     * @param listener
     */
    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener){
        mMedialPlayer.setOnCompletionListener(listener);
    }

    /**
     * 播放错误
     * @param listener
     */
    public void setOnErrorListener(MediaPlayer.OnErrorListener listener){
        mMedialPlayer.setOnErrorListener(listener);
    }

    /**
     * 播放进度
     * @param listener
     */
    public void setOnProgressListener(OnMusicProgressListener listener){
        musicProgressListener=listener;
    }

    public interface OnMusicProgressListener{
        void onProgress(int progress,int pos);
    }
}
