package com.dayuanshu.framework.manager;

import android.content.Context;
import android.view.Gravity;

import com.dayuanshu.framework.R;
import com.dayuanshu.framework.view.DialogView;

/**
 * 提示框管理类
 */
public class DialogManager {

    private static volatile DialogManager mInstance=null;

    private DialogManager(){}

    public static DialogManager getInstance(){
        if(mInstance==null){
            synchronized (DialogManager.class){
                if(mInstance==null){
                    mInstance=new DialogManager();
                }
            }
        }
        return mInstance;
    }

    public DialogView initView(Context mContext,int layout,int gravity){
        return new DialogView(mContext,layout, R.style.Theme_Dialog,gravity);
    }
    public DialogView initView(Context mContext,int layout){
        return new DialogView(mContext,layout, R.style.Theme_Dialog, Gravity.CENTER);
    }

    /**
     * Dialog对话框显示
     * @param view
     */
    public void show(DialogView view){
        if(view!=null){
            if(!view.isShowing()){
                view.show();
            }
        }
    }

    /**
     * Dialog对话框隐藏
     * @param view
     */
    public void hide(DialogView view){
        if(view!=null){
            if(view.isShowing()){
                view.dismiss();
            }
        }
    }
}
