package com.dayuanshu.framework.model;

/**
 * 聊天的数据模型
 */
public class ChatModel {

    private int type;
    private String text;

    public int getType(){
        return type;
    }
    public void setType(int type){
        this.type=type;
    }
    public String getText(){
        return text;
    }
    public void setText(String text){
        this.text=text;
    }
}
