package com.dayuanshu.framework.manager;

import com.dayuanshu.framework.cloud.CloudManager;
import com.dayuanshu.framework.utils.SHA1;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * OKHttp
 */
public class HttpManager {

    private static volatile HttpManager httpManager =null;
    private OkHttpClient okHttpClient;

    private HttpManager(){
        okHttpClient = new OkHttpClient();
    }

    public static HttpManager getInstance(){
        if(httpManager == null){
            synchronized (HttpManager.class){
                if(httpManager == null){
                    httpManager = new HttpManager();
                }
            }
        }
        return httpManager;
    }

    /**
     * 请求融云token
     * */
    public String postCloudToken(HashMap<String,String> map){
        //参数
        String Timestamp = String.valueOf(System.currentTimeMillis() /1000);
        String Nonce = String.valueOf(Math.floor(Math.random()*100000));
        String Signature = SHA1.sha1(CloudManager.CLOUD_SECRET+Nonce+Timestamp);

        //参数填充
        FormBody.Builder builder = new FormBody.Builder();
        for(String key:map.keySet()){
            builder.add(key,map.get(key));
        }
        RequestBody requestBody = builder.build();
        //添加签名规则
        Request request = new Request.Builder()
                .url(CloudManager.TOKEN_URL)
                .addHeader("Timestamp",Timestamp)
                .addHeader("App-Key",CloudManager.CLOUD_KEY)
                .addHeader("Nonce",Nonce)
                .addHeader("Signature",Signature)
                .addHeader("Content-Type","application/x-www-form-urlencoded")
                .post(requestBody)
                .build();
        try {
            return okHttpClient.newCall(request).execute().body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }
}
