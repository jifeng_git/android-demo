package com.dayuanshu.framework.utils;

/**
 * 时间转换类
 */
public class TimeUtils {

    /**
     * 转换毫秒格式: HH:mm:ssS
     * 1s=1000S
     * 1m=60s
     * 1h=60m
     * 1d=24h
     * @param ms
     */
    public static String formatDuring(long ms){
        // 时
        long hours=(ms%(1000*60*60*24))/(1000*60*60);
        // 分钟
        long minutes=(ms%(1000*60*60))/(1000*60);
        // 秒钟
        long seconds=(ms%(1000*60))/1000;
        String h=hours+"";
        if(hours<10){
            h="0"+h;
        }
        String m=minutes+"";
        if(minutes<10){
            m="0"+m;
        }
        String s=seconds+"";
        if(seconds<10){
            s="0"+s;
        }
        return h+":"+m+":"+s;
    }
}
