package com.dayuanshu.framework.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.core.content.FileProvider;
import androidx.loader.content.CursorLoader;

import com.dayuanshu.framework.utils.LogUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 关于文件的帮助类
 */
public class FileHelper {
    //相机
    public static final int CAMEAR_REQUEST_CODE = 1004;

    //相册
    public static final int ALBUM_REQUEST_CODE = 1005;

    private static volatile FileHelper mInstance = null;

    private File tempFile = null;
    private Uri imageUri;

    private SimpleDateFormat simpleDateFormat;

    private FileHelper(){
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss");
    }


    public File getTimeFile(){
        return tempFile;
    }

    public static FileHelper getInstance(){
        if (mInstance == null){
            synchronized (FileHelper.class){
                if (mInstance == null){
                    mInstance = new FileHelper();
                }
            }
        }
        return mInstance;
    }



    /**
     * 相机
     * */
    public void toCamera(Activity mActivity){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String fileName =simpleDateFormat.format(new Date());
        tempFile = new File(Environment.getExternalStorageDirectory(),fileName+".jpg");
        //兼容Android N
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            imageUri = Uri.fromFile(tempFile);
        } else {
            //利用FileProvider
            imageUri = FileProvider.getUriForFile(mActivity,
                    mActivity.getPackageName()+ ".fileprovider",tempFile);
            //添加权限
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION |
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            );
        }
        LogUtils.i("imageUri:" + imageUri);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);
        mActivity.startActivityForResult(intent,CAMEAR_REQUEST_CODE);
    }

    /**
     * 跳转到相册
     * */
    public void toAlbum(Activity mactivity){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        mactivity.startActivityForResult(intent,ALBUM_REQUEST_CODE);
    }

    /**
     * 通过uri去系统查询真实地址
     * */
    public String getRealPathFromURI(Context context,Uri uri){
        String [] proj = {MediaStore.Images.Media.DATA} ;
        CursorLoader cursorLoader = new CursorLoader(context, uri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();
        int index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(index);
    }


}
