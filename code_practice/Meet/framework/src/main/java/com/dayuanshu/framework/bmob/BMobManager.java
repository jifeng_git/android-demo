package com.dayuanshu.framework.bmob;

import android.content.Context;

import com.dayuanshu.framework.utils.CommonUtils;

import java.io.File;
import java.util.List;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobSMS;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.LogInListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import cn.bmob.v3.listener.UploadFileListener;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;

/**
 * 管理类
 */
public class BMobManager {

    //private static final String BMOB_SDK_ID = "adf8e8d7a9d02059169523ed1e9f3bbb";
    // 罗明兴
    private static final String BMOB_SDK_ID = "7e6b42a3cb71ec9fd6a83241ae2168d1";
    // 刘某人 code_practice
    //private static final String BMOB_SDK_ID = "f8efae5debf319071b44339cf51153fc";

    private volatile static BMobManager mInstance=null;

    private BMobManager(){}

    public static BMobManager getInstance(){
        if(mInstance==null){
            synchronized (BMobManager.class){
                mInstance=new BMobManager();
            }
        }
        return mInstance;
    }

    /**
     * 查询本地的会话记录
     * */
    public void getConversationList(RongIMClient.ResultCallback<List<Conversation>> callback){
        RongIMClient.getInstance().getConversationList(callback);
    }

    /**
     * 初始化Bmob
     * @param context
     */
    public void initBMob(Context context){
        Bmob.initialize(context, BMOB_SDK_ID);
    }
    /**
     * 获取本地对象
     * @return
     */
    public IMUser getUser(){
        return BmobUser.getCurrentUser(IMUser.class);
    }
    /**
     * 发送短信验证码
     * @param phone 手机号
     * @param listener 回调
     */
    public void requestSMS(String phone, QueryListener<Integer> listener){
        BmobSMS.requestSMSCode(phone,"",listener);
    }

    /**
     * 通过手机号码一键注册或登录
     * @param phone 手机号
     * @param code 验证码
     * @param listener 回调
     */
    public void signOrLoginByMobilePhone(String phone, String code, LogInListener<IMUser> listener){
        BmobUser.signOrLoginByMobilePhone(phone,code,listener);
    }



    /**
     * 判断用户是否登录
     * @return
     */
    public boolean isLogin(){
        return BmobUser.isLogin();
    }

    /**
     * 第一次上传头像
     */
    public void uploadFirstPhoto(String nickName, File file,OnUploadPhotoListener listener) {
        /**
         * 1.上传文件拿到地址
         * 2.更新用户信息
         *
         */
        final IMUser user = getUser();
        /**
         * 独立域名 设置--应用设置 -独立域名 一年/100
         * 免费办法:使用老师的key
         * 若怕次数冲突
         * 解决办法:跟老师的类名不一样即可
         */
        final BmobFile bmobFile=new BmobFile(file);
        bmobFile.uploadblock(new UploadFileListener() {
            @Override
            public void done(BmobException e) {
                if (e==null) {
                    // 上传成功
                    user.setNickName(nickName);
                    user.setPhoto(bmobFile.getFileUrl());

                    user.setTokenNickName(nickName);
                    user.setTokenPhoto(bmobFile.getFileUrl());

                    //更新用户信息
                    user.update(new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if (e==null) {
                                listener.onUpdateDone();
                            }else {
                                listener.onUpdateFail(e);
                            }
                        }
                    });
                }else {
                    listener.onUpdateFail(e);
                }
            }
        });
    }


    public interface OnUploadPhotoListener{
        void onUpdateDone();
        void onUpdateFail(BmobException e);
    }

    /**
     * 根据电话号码查询用户信息
     * @param phone
     */
    public void queryPhoneUser(String phone, FindListener<IMUser> listener){
        baseQuery("mobilePhoneNumber",phone,listener);
    }

    /**
     * 查询我的好友
     * @param listener
     */
    public void queryMyFriends(FindListener<Friend> listener){
        BmobQuery<Friend> query=new BmobQuery<>();
        query.addWhereEqualTo("user",getUser());
        query.findObjects(listener);
    }

    /**
     * 根据objectId查询用户
     * @param objectId
     * @param listener
     */
    public void queryObjectIdUser(String objectId, FindListener<IMUser> listener){
        baseQuery("objectId",objectId,listener);
    }

    /**
     * 查询所有的用户
     * @param listener
     */
    public void queryAllUser(FindListener<IMUser> listener){
        BmobQuery<IMUser> query=new BmobQuery<>();
        query.findObjects(listener);
    }
    /**
     * 查询的基类
     * @param key
     * @param value
     * @param listener
     */
    public void baseQuery(String key,String value,FindListener<IMUser> listener){
        BmobQuery<IMUser> query=new BmobQuery<>();
        query.addWhereEqualTo(key,value);
        query.findObjects(listener);
    }

    /**
     * 添加好友
     * @param imUser
     * @param listener
     */
    public void addFriend(IMUser imUser, SaveListener<String> listener) {
        Friend friend = new Friend();
        friend.setUser(getUser());
        friend.setFriendUser(imUser);
        friend.save(listener);
    }

    /**
     * 通过Id添加好友
     * @param id
     * @param listener
     */
    public void addFriendById(String id,SaveListener<String> listener){
        queryObjectIdUser(id, new FindListener<IMUser>() {
            @Override
            public void done(List<IMUser> list, BmobException e) {
                if (e==null) {
                    if (CommonUtils.isEmpty(list)) {
                        IMUser imUser = list.get(0);
                        addFriend(imUser,listener);
                    }
                }
            }
        });
    }
}
