package com.dayuanshu.framework.entity;

/**
 * 常量类
 */
public class Constants {

    /**
     * 是否第一次进入app
     */
    public static final String SP_IS_FIRST_APP="is_First_App";
    /**
     * token
     */
    public static final String SP_TOKEN = "token";

    /**
     * 手机号码
     */
    public static final String SP_PHONE = "phone";
    // intent传递
    public static final String INTENT_USER_ID = "intent_user_id";
    public static final String INTENT_USER_NAME = "intent_user_name";
    public static final String INTENT_USER_PHOTO = "intent_user_photo";
}
