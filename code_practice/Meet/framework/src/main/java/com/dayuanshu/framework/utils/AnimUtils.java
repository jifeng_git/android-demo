package com.dayuanshu.framework.utils;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * 动画工具类
 */
public class AnimUtils {

    /**
     * 旋转动画
     * @param view
     * @return
     */
    public static ObjectAnimator rotation(View view){
        // 旋转动画
        ObjectAnimator mAnim=ObjectAnimator.ofFloat(view,"rotation",0f,360f);
        // 旋转时间设置为2秒
        mAnim.setDuration(2*1000);
        // 模式:循环播放
        mAnim.setRepeatMode(ValueAnimator.RESTART);
        // 重复播放多少次
        mAnim.setRepeatCount(ValueAnimator.INFINITE);
        // 动画经过的部分的时间插值器
        mAnim.setInterpolator(new LinearInterpolator());
        return mAnim;
    }
}
