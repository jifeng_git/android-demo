package com.dayuanshu.framework.utils;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.view.View;

/**
 * 沉浸式实现
 * Android 5.0
 * getWindow().getDecorView()
 *            .setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
 *            |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
 *
 * getWindow().getDecorView()
 *            .setSystemUiVisibility(
 *            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
 *            |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
 *            |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
 *            |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
 *            |View.SYSTEM_UI_FLAG_FULLSCREEN
 *            |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
 * 隐藏状态栏
 * getSupportActionBar().hide();
 * 颜色设置透明
 * getWindow().setStatusBarColor(Color.TRANSPARENT);
 */
public class SystemUI {

    public static void fixSystemUI(Activity mActivity){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            // 获取最顶层的View
            mActivity.getWindow().getDecorView()
                    .setSystemUiVisibility(
                     View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                     |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            // 颜色设置透明
            mActivity.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
