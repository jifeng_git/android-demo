package com.dayuanshu.framework;

import android.content.Context;

import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.cloud.CloudManager;
import com.dayuanshu.framework.utils.LogUtils;
import com.dayuanshu.framework.utils.SpUtils;
import org.litepal.LitePal;
/**
 * FrameWork入口
 */
public class FrameWork {

    private static volatile FrameWork mFrameWork;

    private FrameWork(){}

    public static FrameWork getFrameWork(){
        if(mFrameWork==null){
            synchronized (FrameWork.class){
                mFrameWork=new FrameWork();
            }
        }
        return mFrameWork;
    }

    /**
     * 初始化框架 Bmob Model
     * @param mContext
     */
    public void initFramework(Context mContext){
        LogUtils.e("initFramework111");
        SpUtils.getInstance().initSp(mContext);
        // 初始化Bmob
        BMobManager.getInstance().initBMob(mContext);
        // 初始化融云
        CloudManager.getInstance().initCloud(mContext);
        // 初始化LitePal数据库
        LitePal.initialize(mContext);
    }
}
