package com.dayuanshu.framework.db;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;

import java.util.List;

/**
 * 本地数据库帮助类
 */
public class LitePalHelper {

    private static volatile LitePalHelper mInstance=null;

    private LitePalHelper(){}

    public static LitePalHelper getInstance(){
        if (mInstance==null) {
            synchronized (LitePalHelper.class){
                if (mInstance==null) {
                    mInstance=new LitePalHelper();
                }
            }
        }
        return mInstance;
    }

    /**
     * 保存的基类
     * @param support
     */
    private void baseSave(LitePalSupport support){
        support.save();
    }

    /**
     * 保存新朋友
     * @param msg
     * @param id
     */
    public void saveNewFriend(String msg,String id){
        NewFriend friend = new NewFriend();
        friend.setId(id);
        friend.setMsg(msg);
        friend.setSaveTime(System.currentTimeMillis());
        friend.setIsAgree(-1);
        baseSave(friend);
    }

    /**
     * 查询的基类
     * @param cls
     * @return
     */
    private List<?extends LitePalSupport>  baseQuery(Class cls){
        return LitePal.findAll(cls);
    }

    /**
     * 查询新朋友
     * @return
     */
    public List<NewFriend> queryNewFriend(){
        return (List<NewFriend>) baseQuery(NewFriend.class);
    }

    /**
     * 更新新朋友的数据库状态
     * @param userId
     * @param agree
     */
    public void updateNewFriend(String userId,int agree){
        NewFriend newFriend = new NewFriend();
        newFriend.setIsAgree(agree);
        newFriend.updateAll("userId = ?",userId);
    }
}
