package com.dayuanshu.framework.utils;

import android.icu.text.SimpleDateFormat;
import android.text.TextUtils;
import android.util.Log;

import com.dayuanshu.framework.BuildConfig;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Date;


/**
 * 静态日志工具类
 * Log不光作为日志的打印,还可以记录日志-->通过File记录
 */
public class LogUtils {
    private  static SimpleDateFormat mSimpleDateFormat=new SimpleDateFormat("YYYY-MM-DD HH:mm:ss");
    //LogUtils.i(""),每次调用,都会判断是否为空re
    public static void i(String text){
        if (BuildConfig.LOG_DEBUG) {
            if(!TextUtils.isEmpty(text)){
                Log.i(BuildConfig.LOG_TAG,text);
//                writeToFile(text);
            }
        }
    }

    public static void e(String text){
        if (BuildConfig.LOG_DEBUG) {
            if(!TextUtils.isEmpty(text)){
                Log.e(BuildConfig.LOG_TAG,text);
//                writeToFile(text);
            }
        }
    }

    /**
     * 将Log日志写入磁盘
     * @param text
     */
    private static void writeToFile(String text){
        // 文件路径
//        String fileName="E:\\logs\\Meet.log";
        String fileName="C:\\Users\\admin\\.android\\avd\\New_Device_2_API_28.avd\\data\\Meet.log";
        // 时间+内容
        String log=mSimpleDateFormat.format(new Date())+" "+text+"\n";
        // 检查路径是否存在
//        File fileGroup=new File("E:\\logs\\");
        File fileGroup=new File("C:\\Users\\admin\\.android\\avd\\New_Device_2_API_28.avd\\data");
        if(!fileGroup.exists()){
            fileGroup.mkdirs();
        }

        // 开始写入
        FileOutputStream outputStream = null;
        BufferedWriter bufferedWriter = null;
        try {
            outputStream=new FileOutputStream(fileName,true);
            // 编码问题,GBK能正确存入中文
            bufferedWriter
                    = new BufferedWriter(new OutputStreamWriter(outputStream, Charset.forName("gbk")));
            bufferedWriter.write(log);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(bufferedWriter!=null){
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
