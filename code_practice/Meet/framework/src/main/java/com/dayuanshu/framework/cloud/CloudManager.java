package com.dayuanshu.framework.cloud;

import android.content.Context;

import com.dayuanshu.framework.utils.LogUtils;


import org.json.JSONObject;

import java.util.List;

import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

/**
 * 融云管理
 */
public class CloudManager {

    // 刘斌融云key
    //Url
    public static final String TOKEN_URL = "http://api-cn.ronghub.com/user/getToken.json";
    //Key
    //public static final String CLOUD_KEY = "p5tvi9dspkar4";
    public static final String CLOUD_KEY = "0vnjpoad0m1uz";
    //令牌
    //public static final String CLOUD_SECRET = "xqvMKp9fL68Z3H";
    public static final String CLOUD_SECRET = "UEGupyXvUho";

    //ObjectName
    public static final String MSG_TEXT_NAME = "RC:TxtMsg";
    public static final String MSG_IMAGE_NAME = "RC:ImgMsg";
    public static final String MSG_LOCATION_NAME = "RC:LBSMsg";

    // 消息类型
    // 添加好友消息
    public static final String TYPE_ADD_FRIEND="TYPE_ADD_FRIEND";
    // 普通消息
    public static final String TYPE_TEXT="TYPE_TEXT";
    // 同意添加好友
    public static final String TYPE_AGREE_FRIEND="TYPE_AGREE_FRIEND";

    private static volatile CloudManager cloudManager;

    private CloudManager(){}

    public static CloudManager getInstance(){
        if(cloudManager == null){
            synchronized (CloudManager.class){
                if (cloudManager ==null){
                    cloudManager = new CloudManager();
                }
            }
        }
        return cloudManager;
    }

    /**
     * 初始化sdk
     * */
    public void initCloud(Context context){
        RongIMClient.init(context,"0vnjpoad0m1uz",false);
    }

    /**
     * 连接融云服务
     * */
    public void connect(String token){
        RongIMClient.connect(token, new RongIMClient.ConnectCallback() {
            @Override
            public void onSuccess(String s) {
                LogUtils.e("连接成功" + s);
                //启动模拟器发送消息给手机，消息内容是 你好
                CloudManager.getInstance().sendTextMessage("你好","84d6405fb6");

            }

            @Override
            public void onError(RongIMClient.ConnectionErrorCode connectionErrorCode) {
                LogUtils.e("连接失败" + connectionErrorCode);
            }

            @Override
            public void onDatabaseOpened(RongIMClient.DatabaseOpenStatus databaseOpenStatus) {
                LogUtils.e("Token Error");
            }
        });
    }

    /**
     * 断开连接
     */
    public void disconnect(){
        RongIMClient.getInstance().disconnect();
    }

    /**
     * 退出登录
     */
    public void logout(){
        RongIMClient.getInstance().logout();
    }

    /**
     * 接收消息的监听器
     * */
    public void setOnReceiveMessageListener(io.rong.imlib.IRongCoreListener.OnReceiveMessageListener listener){
        RongIMClient.setOnReceiveMessageListener(listener);
    }

    /**
     * 发送消息的结果回调
     */
    private IRongCallback.ISendMessageCallback iSendMessageCallback = new IRongCallback.ISendMessageCallback(){

        @Override
        public void onAttached(Message message) {
            //消息成功存到本地数据库的回调
            LogUtils.i("消息存入本地数据库了...........");
        }

        @Override
        public void onSuccess(Message message) {
            //消息发送成功的回调
            LogUtils.i("sendMessage onSuccess");

        }

        @Override
        public void onError(Message message, RongIMClient.ErrorCode errorCode) {
            //消息发送失败的回调
            LogUtils.i("sendMessage onError:"+ errorCode);
        }
    };

    /**
     * 发送文本消息
     * */
    private void sendTextMessage(String msg,String targetId){
        LogUtils.i("sendMessage");
        TextMessage textMessage = TextMessage.obtain(msg);
        RongIMClient.getInstance().sendMessage(
                Conversation.ConversationType.PRIVATE,
                targetId,
                textMessage,
                null,
                null,iSendMessageCallback
        );
    }

    /**
     * 发送添加好友消息和普通消息
     * @param msg
     * @param type
     * @param targetId
     */
    public void sendTextMessage(String msg,String type,String targetId){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msg",msg);
            // 如果没有这个type，就是一个普通消息
            jsonObject.put("type",type);
            sendTextMessage(jsonObject.toString(),targetId);
        }catch (Exception e){
            e.getStackTrace();
        }
    }

    /**
     * 查询本地的会话记录
     * @param callback
     */
    public void getConversationList(RongIMClient.ResultCallback<List<Conversation>> callback){
        RongIMClient.getInstance().getConversationList(callback);
    }

    /**
     * 加载本地历史记录
     * @param targetId
     * @param callback
     */
    public void getHistoryMessages(String targetId, RongIMClient.ResultCallback<List<Message>> callback) {
        RongIMClient.getInstance().getHistoryMessages(Conversation.ConversationType.PRIVATE
                , targetId, -1, 1000, callback);
    }

    /**
     * 获取服务器的历史记录
     * @param targetId
     * @param callback
     */
    public void getRemoteHistoryMessages(String targetId, RongIMClient.ResultCallback<List<Message>> callback) {
        RongIMClient.getInstance().getRemoteHistoryMessages(Conversation.ConversationType.PRIVATE
                , targetId, 0, 20, callback);
    }

}
