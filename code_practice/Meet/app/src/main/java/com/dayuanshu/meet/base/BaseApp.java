package com.dayuanshu.meet.base;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;

import com.dayuanshu.framework.FrameWork;


public class BaseApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // 只在主进程中初始化
        if (getApplicationInfo().packageName.equals(getCurProcessName(getApplicationContext()))) {
            FrameWork.getFrameWork().initFramework(this);
        }

        /**
         * Application的优化
         * 1.必要的组件在程序主页去初始化
         * 2.如果组件一定要在App中初始化，那么尽可能的延时
         * 3.非必要的组件,子线程中初始化
         */
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                // 调用Bmob
                FrameWork.getFrameWork().initFramework(BaseApp.this);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FrameWork.getFrameWork().initFramework(BaseApp.this);
            }
        },2000);*/
        //FrameWork.getFrameWork().initFramework(BaseApp.this);

    }

    /**
     * 获取当前进程名称
     * @param mContext
     * @return
     */
    public static String getCurProcessName(Context mContext){
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager)
                mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess :
                activityManager.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }
}
