package com.dayuanshu.meet.fragment.chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dayuanshu.framework.adapter.CommonAdapter;
import com.dayuanshu.framework.adapter.CommonViewHolder;
import com.dayuanshu.framework.base.BaseFragment;
import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.bmob.IMUser;
import com.dayuanshu.framework.cloud.CloudManager;
import com.dayuanshu.framework.gson.TextBean;
import com.dayuanshu.framework.model.ChatRecordModel;
import com.dayuanshu.framework.utils.CommonUtils;
import com.dayuanshu.meet.R;
import com.dayuanshu.meet.ui.ChatActivity;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.message.TextMessage;

/**
 * 聊天记录
 */
public class ChatRecordFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private View item_empty_view;

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private CommonAdapter<ChatRecordModel> adapter;
    private List<ChatRecordModel> mList = new ArrayList();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_record, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        item_empty_view = view.findViewById(R.id.item_empty_view);
        swipeRefreshLayout = view.findViewById(R.id.mChatRecordRefreshLayout);
        recyclerView = view.findViewById(R.id.mChatRecordView);

        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));

        adapter = new CommonAdapter<>(mList, new CommonAdapter.OnBindDataListener<ChatRecordModel>() {
            @Override
            public void onBindViewHolder(ChatRecordModel model, CommonViewHolder viewHolder, int type, int position) {
                viewHolder.setImageUrl(getActivity(),R.id.iv_photo,model.getUrl());
                viewHolder.setText(R.id.tv_nickname,model.getNickName());
                viewHolder.setText(R.id.tv_content,model.getEndMsg());
                viewHolder.setText(R.id.tv_time,model.getTime());

                if (model.getUnReadSize() == 0 ){
                    viewHolder.getView(R.id.tv_un_read).setVisibility(View.GONE);
                }else {
                    viewHolder.getView(R.id.tv_un_read).setVisibility(View.VISIBLE);
                    viewHolder.setText(R.id.tv_un_read,model.getUnReadSize()+ "");
                }

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ChatActivity.startActivity(getActivity(),
                                model.getUserId(),model.getNickName(),model.getUrl());
                    }
                });


            }

            @Override
            public int getLayoutId(int type) {
                return R.layout.layout_chat_record_item;
            }
        });
        recyclerView.setAdapter(adapter);

        queryChatRecord();

    }

    /**
     * 查询聊天记录
     * */
    private void queryChatRecord() {
        swipeRefreshLayout.setRefreshing(true);
        BMobManager.getInstance().getConversationList(new RongIMClient.ResultCallback<List<Conversation>>() {
            @Override
            public void onSuccess(List<Conversation> conversations) {
                swipeRefreshLayout.setRefreshing(false);
                if (CommonUtils.isEmpty(conversations)){
                    if(mList.size()> 0 ){
                        mList.clear();
                    }

                    for(int i= 0;i<conversations.size();i++){
//                        LogUtils.i("conversations：" + conversations.get(i).toString());
                        Conversation c = conversations.get(i);
                        String id = c.getTargetId();
                        //查询对象的信息
                        BMobManager.getInstance().queryObjectIdUser(id, new FindListener<IMUser>() {
                            @Override
                            public void done(List<IMUser> list, BmobException e) {
                                if(e == null){
                                    if(CommonUtils.isEmpty(list)){
                                        IMUser imUser = list.get(0);
                                        ChatRecordModel chatRecordMmodel = new ChatRecordModel();
                                        chatRecordMmodel.setUrl(imUser.getPhoto());
                                        chatRecordMmodel.setNickName(imUser.getNickName());
                                        chatRecordMmodel.setTime(new SimpleDateFormat("HH:mm:ss")
                                                .format(c.getReceivedTime()));
                                        chatRecordMmodel.setUnReadSize(c.getUnreadMessageCount());

                                        String objectName = c.getObjectName();
                                        if (objectName.equals(CloudManager.MSG_TEXT_NAME)){
                                            TextMessage textMessage = (TextMessage) c.getLatestMessage();
                                            String msg = textMessage.getContent();
                                            TextBean textBean = new Gson().fromJson(msg,TextBean.class);
                                            if (textBean.getType().equals(CloudManager.TYPE_TEXT)){
                                                chatRecordMmodel.setEndMsg(textBean.getMsg());
                                                mList.add(chatRecordMmodel);
                                            }
                                        }else if (objectName.equals(CloudManager.MSG_TEXT_NAME)){
                                            chatRecordMmodel.setEndMsg(getString(R.string.text_chat_record_img));
                                            mList.add(chatRecordMmodel);
                                        }else if (objectName.equals(CloudManager.MSG_LOCATION_NAME)){
                                            chatRecordMmodel.setEndMsg(getString(R.string.text_chat_record_location));
                                            mList.add(chatRecordMmodel);
                                        }

                                        adapter.notifyDataSetChanged();

                                        if (mList.size()> 0){
                                            item_empty_view.setVisibility(View.GONE);
                                            recyclerView.setVisibility(View.VISIBLE);
                                        }else {
                                            item_empty_view.setVisibility(View.VISIBLE);
                                            recyclerView.setVisibility(View.GONE);
                                        }
                                    }
                                }
                            }
                        });
                    }
                }else {
                    swipeRefreshLayout.setRefreshing(false);
                    item_empty_view.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    @Override
    public void onRefresh() {
        if(swipeRefreshLayout.isRefreshing()){
            queryChatRecord();
        }
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(MessageEvent event) {
//        switch (event.getType()) {
//            case EventManager.FLAG_UPDATE_FRIEND_LIST:
//                if (swipeRefreshLayout.isRefreshing()) {
//                    queryChatRecord();
//                }
//                break;
//        }
//    }

}
