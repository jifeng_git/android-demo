package com.dayuanshu.meet.ui;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dayuanshu.framework.adapter.CommonAdapter;
import com.dayuanshu.framework.adapter.CommonViewHolder;
import com.dayuanshu.framework.base.BaseBackActivity;
import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.bmob.IMUser;
import com.dayuanshu.framework.model.AddFriendModel;
import com.dayuanshu.framework.utils.CommonUtils;
import com.dayuanshu.framework.utils.LogUtils;
import com.dayuanshu.meet.R;
import com.dayuanshu.meet.adapter.AddFriendAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * FileName: ContactFirendActivity
 * Founder: LiuGuiLin
 * Profile: 从通讯录导入
 */
public class ContactFirendActivity extends BaseBackActivity {

    private Disposable disposable;

    // 读取联系人
    private RecyclerView mContactView;
    private Map<String, String> mContactMap = new HashMap<>();
    //private AddFriendAdapter mAddFriendAdapter;
    private CommonAdapter<AddFriendModel> mContactAdapter;
    private List<AddFriendModel> mList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_friend);

        initView();
    }

    /**
     * 初始化View
     */
    /*private void initView() {
        mContactView = (RecyclerView) findViewById(R.id.mContactView);
        mContactView.setLayoutManager(new LinearLayoutManager(this));
        mContactView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mAddFriendAdapter=new AddFriendAdapter(this,mList);

        // 设置点击事件
        mAddFriendAdapter.setOnClickListener(new AddFriendAdapter.OnClickListener() {
            @Override
            public void onClick(int position) {

            }
        });
        loadContact();
        mContactView.setAdapter(mAddFriendAdapter);
        loadUser();
    }*/

    private void initView() {
        mContactView = (RecyclerView) findViewById(R.id.mContactView);
        mContactView.setLayoutManager(new LinearLayoutManager(this));
        mContactView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        mContactAdapter = new CommonAdapter<>(mList, new CommonAdapter.OnBindDataListener<AddFriendModel>() {
            @Override
            public void onBindViewHolder(final AddFriendModel model, CommonViewHolder viewHolder, int type, int position) {
                //设置头像
                viewHolder.setImageUrl(ContactFirendActivity.this, R.id.iv_photo, model.getPhoto());
                //设置性别
                viewHolder.setImageResource(R.id.iv_sex,
                        model.isSex() ? R.drawable.img_boy_icon : R.drawable.img_girl_icon);
                //设置昵称
                viewHolder.setText(R.id.tv_nickname, model.getNickName());
                //年龄
                viewHolder.setText(R.id.tv_age, model.getAge() + getString(R.string.text_search_age));
                //设置描述
                viewHolder.setText(R.id.tv_desc, model.getDesc());

                if (model.isContact()) {
                    viewHolder.getView(R.id.ll_contact_info).setVisibility(View.VISIBLE);
                    viewHolder.setText(R.id.tv_contact_name, model.getContactName());
                    viewHolder.setText(R.id.tv_contact_phone, model.getContactPhone());
                }

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserInfoActivity.startActivity(ContactFirendActivity.this,
                                model.getUserId());
                    }
                });
            }

            @Override
            public int getLayoutId(int type) {
                return R.layout.layout_search_user_item;
            }
        });

        mContactView.setAdapter(mContactAdapter);

        loadUser();
    }

    /**
     * 加载用户
     */
    private void loadUser() {
        if (mContactMap.size()>0) {
            for (Map.Entry<String, String> stringEntry : mContactMap.entrySet()) {
                BMobManager.getInstance().queryPhoneUser(stringEntry.getValue(), new FindListener<IMUser>() {
                    @Override
                    public void done(List<IMUser> list, BmobException e) {
                        if (e==null) {
                            if (CommonUtils.isEmpty(list)) {
                               IMUser imUser= list.get(0);
                               addContent(imUser,stringEntry.getKey(),stringEntry.getValue());
                            }
                        }
                    }
                });
            }
        }
    }

    /**
     * 加载联系人
     */
    @SuppressLint("Range")
    private void loadContact() {
       Cursor cursor= getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        ,null,null,null,null);
       String name;
       String phone;
        while (cursor.moveToNext()) {
            name=cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            phone=cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phone=phone.replace(" ","").replace("-","");
            LogUtils.e("name:"+name+"<=======>phone:"+phone);
            // 存储
            mContactMap.put(name,phone);
        }
    }

    /**
     * 添加内容
     * */
    private void addContent(IMUser imUser,String name,String phone){
        AddFriendModel model = new AddFriendModel();
        model.setType(AddFriendAdapter.TYPE_CONTENT);
        model.setUserId(imUser.getObjectId());
        model.setPhoto(imUser.getPhoto());
        model.setSex(imUser.isSex());
        model.setNickName(imUser.getNickName());
        model.setAge(imUser.getAge());
        model.setDesc(imUser.getDesc());
        model.setContact(true);
        model.setContactName(name);
        model.setContactPhone(phone);
        mList.add(model);

        // 每次加载完,通过适配器去刷新一下
        //mAddFriendAdapter.notifyDataSetChanged();
        mContactAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable();
    }

    //解除绑定
    private void disposable() {
        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }
        }
    }

    @Override
    public void onBackPressed() {
        disposable();
        super.onBackPressed();
    }

}
