package com.dayuanshu.meet.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dayuanshu.framework.base.BaseUIActivity;
import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.bmob.IMUser;
import com.dayuanshu.framework.entity.Constants;
import com.dayuanshu.framework.manager.DialogManager;
import com.dayuanshu.framework.utils.SpUtils;
import com.dayuanshu.framework.view.DialogView;
import com.dayuanshu.framework.view.LoadingView;
import com.dayuanshu.framework.view.TouchPictureV;
import com.dayuanshu.meet.MainActivity;
import com.dayuanshu.meet.R;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.LogInListener;
import cn.bmob.v3.listener.QueryListener;

/**
 * 登录页
 */
public class LoginActivity extends BaseUIActivity implements View.OnClickListener {

    /**
     * 1.点击发送的按钮，弹出一个提示框，图片验证码，验证通过之后
     * 2.!发送验证码，@同时按钮变成不可点击，@按钮开始倒计时，倒计时结束，@按钮可点击，@文字变成“发送”
     * 3.通过手机号码和验证码进行登录
     * 4.登录成功之后获取本地对象
     */
    /*
    * 1.点击发送按钮,弹出一个提示框,图片验证码,验证通过之后
    * 2.发送验证码 (1)同时按钮变成不可点击,按钮倒计时,(2)倒计时结束,按钮可点击,文字变成"发送"
    * */
    private EditText et_phone;
    private EditText et_code;
    private Button btn_send_code;
    private Button btn_login;

    private DialogView mCodeView;
    private TouchPictureV mPictureV;

    private TextView tv_test_login;

    // 加载动画
    private LoadingView mLoadingView;

    // what
    private static final int H_TIME = 1001;

    private Handler mHandler=new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case H_TIME:
                    // 描述递减
                    TIME--;
                    // 发送验证的变成文字提示
                    btn_send_code.setText(TIME+"s");
                    if(TIME>0){
                        mHandler.sendEmptyMessageDelayed(H_TIME,1000);
                    }else {
                        // 发送验证码按钮变成可点击
                        btn_send_code.setEnabled(true);
                        // 文字变成发送按钮
                        btn_send_code.setText(getString(R.string.text_login_send));
                    }
                    break;
            }
            return false;
        }
    });

    // 60s倒计时
    private static int TIME = 60;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        // dialog对话框
        initDialogView();
        // 的事登录按钮
        tv_test_login = findViewById(R.id.tv_test_login);
        // 测试登录点击事件
        tv_test_login.setOnClickListener(this);

        // 手机号码
        et_phone = (EditText) findViewById(R.id.et_phone);
        // 短信验证码
        et_code = (EditText) findViewById(R.id.et_code);
        // 发送按钮
        btn_send_code = (Button) findViewById(R.id.btn_send_code);
        // 登录按钮
        btn_login = (Button) findViewById(R.id.btn_login);

        // 发送验证码点击事件
        btn_send_code.setOnClickListener(this);
        // 登录点击事件
        btn_login.setOnClickListener(this);

        String phone = SpUtils.getInstance().getString(Constants.SP_PHONE, "");
        if(!TextUtils.isEmpty(phone)){
            // 保存手机号码
            et_phone.setText(phone);
        }

    }

    private void initDialogView() {
        // 加载初始化
        mLoadingView=new LoadingView(this);

        mCodeView= DialogManager.getInstance().initView(this, R.layout.dialog_code_view);
        mPictureV=mCodeView.findViewById(R.id.mPictureV);
        mPictureV.setViewResultListener(new TouchPictureV.OnViewResultListener() {
            @Override
            public void onResult() {
                // 发送短信后隐藏dialog对话框
                DialogManager.getInstance().hide(mCodeView);
                // 验证成功发送短信
                sendSMS();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_code:
                // 发送验证码
                //sendSMS();
                DialogManager.getInstance().show(mCodeView);
                break;
            case R.id.btn_login:
                login();
                //startActivity(new Intent(this, TestActivity.class));
                break;
            case R.id.tv_test_login:
//                startActivity(new Intent(this, TestActivity.class));
                break;
        }
    }

    /**
     * 登录
     */
    private void login() {
        // 1.获取手机号码
        String phone = et_phone.getText().toString().trim();
        // 判断手机号码是否非空
        if(TextUtils.isEmpty(phone)){
            Toast.makeText(this,getString(R.string.text_login_phone_null),Toast.LENGTH_SHORT).show();
            return;
        }
        // 2.获取手机验证码
        String code = et_code.getText().toString().trim();
        // 判断验证码是否非空
        if(TextUtils.isEmpty(code)){
            Toast.makeText(this,getString(R.string.text_login_code_null),Toast.LENGTH_SHORT).show();
            return;
        }
        // 显示Loading
        mLoadingView.show("正在登录中......");
        // 3.进行登录
        BMobManager.getInstance().signOrLoginByMobilePhone(phone, code, new LogInListener<IMUser>() {
            @Override
            public void done(IMUser imUser, BmobException e) {
                if(e==null){
                    // 隐藏加载
                    mLoadingView.hide();
                    // 登录成功
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    // 把手机号码保存下来
                    SpUtils.getInstance().putString(Constants.SP_PHONE,phone);
                    finish();
                }else {
                    // 登录失败
                    Toast.makeText(LoginActivity.this,"登录失败:"+e.toString(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * 发送短信验证码
     */
    private void sendSMS() {
        // 1.获取手机号码
        String phone = et_phone.getText().toString().trim();
        // 验证获取的手机号
        if(TextUtils.isEmpty(phone)){
            Toast.makeText(this,getString(R.string.text_login_phone_null),Toast.LENGTH_SHORT).show();
            return;
        }
        // 手机号码非空,请求短信验证码
        BMobManager.getInstance().requestSMS(phone, new QueryListener<Integer>() {
            @Override
            public void done(Integer integer, BmobException e) {
                if(e==null){
                    // 按钮不可点击
                    btn_send_code.setEnabled(false);
                    mHandler.sendEmptyMessage(H_TIME);
                    Toast.makeText(LoginActivity.this,"短信验证码发送成功",Toast.LENGTH_SHORT).show();;
                }else {
                    Toast.makeText(LoginActivity.this,"短信验证码发送失败",Toast.LENGTH_SHORT).show();;

                }
            }
        });
    }
}
