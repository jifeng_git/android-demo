package com.dayuanshu.meet.ui;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dayuanshu.framework.adapter.CommonAdapter;
import com.dayuanshu.framework.adapter.CommonViewHolder;
import com.dayuanshu.framework.base.BaseBackActivity;
import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.bmob.IMUser;
import com.dayuanshu.framework.model.AddFriendModel;
import com.dayuanshu.framework.utils.CommonUtils;
import com.dayuanshu.framework.utils.LogUtils;
import com.dayuanshu.meet.R;
import com.dayuanshu.meet.adapter.AddFriendAdapter;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * 添加好友
 */
public class AddFriendActivity extends BaseBackActivity implements View.OnClickListener{

    //标题
    public static final int TYPE_TITLE = 0;
    //内容
    public static final int TYPE_CONTENT = 1;

    private LinearLayout ll_to_contact;
    private EditText et_phone;
    private ImageView iv_search;
    private RecyclerView mSearchResultView;

    private View include_empty_view;

    //private AddFriendAdapter addFriendAdapter;

    private CommonAdapter<AddFriendModel> mAddFriendAdapter;
    private List<AddFriendModel> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        initView();
    }

    /**
     * 初始化view
     * */
    private void initView() {

        include_empty_view = findViewById(R.id.include_empty_view);

        ll_to_contact = (LinearLayout) findViewById(R.id.ll_to_contact);

        et_phone = (EditText) findViewById(R.id.et_phone);
        iv_search = (ImageView) findViewById(R.id.iv_search);

        mSearchResultView = (RecyclerView) findViewById(R.id.mSearchResultView);

        ll_to_contact.setOnClickListener(this);
        iv_search.setOnClickListener(this);

        //列表的实现
        mSearchResultView.setLayoutManager(new LinearLayoutManager(this));
        mSearchResultView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // 不需要再去new
        //addFriendAdapter = new AddFriendAdapter(this,mList);


        mAddFriendAdapter = new CommonAdapter<>(mList, new CommonAdapter.OnMoreBindDataListener<AddFriendModel>() {
            @Override
            public int getItemType(int position) {
                return mList.get(position).getType();
            }

            @Override
            public void onBindViewHolder(final AddFriendModel model, CommonViewHolder viewHolder, int type, int position) {
                if (type == TYPE_TITLE) {
                    viewHolder.setText(R.id.tv_title, model.getTitle());
                } else if (type == TYPE_CONTENT) {
                    //设置头像
                    viewHolder.setImageUrl(AddFriendActivity.this, R.id.iv_photo, model.getPhoto());
                    //设置性别
                    viewHolder.setImageResource(R.id.iv_sex,
                            model.isSex() ? R.drawable.img_boy_icon : R.drawable.img_girl_icon);
                    //设置昵称
                    viewHolder.setText(R.id.tv_nickname, model.getNickName());
                    //年龄
                    viewHolder.setText(R.id.tv_age, model.getAge() + getString(R.string.text_search_age));
                    //设置描述
                    viewHolder.setText(R.id.tv_desc, model.getDesc());

                    // 设置点击事件
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            UserInfoActivity.startActivity(AddFriendActivity.this,model.getUserId());
                        }
                    });
                }
            }

            @Override
            public int getLayoutId(int type) {
                if (type == TYPE_TITLE) {
                    return R.layout.layout_search_title_item;
                } else if (type == TYPE_CONTENT) {
                    return R.layout.layout_search_user_item;
                }
                return 0;
            }
        });
        // 设置适配器
        mSearchResultView.setAdapter(mAddFriendAdapter);
    }

    /**
     * 点击事件
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_to_contact:
                // 从通讯录导入
                // 处理权限
                if (checkPermissions(Manifest.permission.READ_CONTACTS)) {
                    startActivity(new Intent(this,ContactFirendActivity.class));
                }else {
                    requestPermission(new String[]{Manifest.permission.READ_CONTACTS});
                }
                break;
            case R.id.iv_search:
                queryPhoneUser();
                break;
        }
    }

    /**
     * 通过电话号码查询
     * */
    private void queryPhoneUser() {
        //1.获取电话号码
        String phone = et_phone.getText().toString().trim();
        if(TextUtils.isEmpty(phone)){
            Toast.makeText(this,getString(R.string.text_login_phone_null),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        // 2.过滤自己
        String phoneNumber=BMobManager.getInstance().getUser().getMobilePhoneNumber();
        LogUtils.e("phoneNumber:"+phoneNumber);
        if (phone.equals(phoneNumber)) {
            Toast.makeText(this,"不能查询自己",Toast.LENGTH_SHORT).show();
            return;
        }
        //3.查询
        BMobManager.getInstance().queryPhoneUser(phone, new FindListener<IMUser>() {
            @Override
            public void done(List<IMUser> list, BmobException e) {
                if (e!=null) {
                    return;
                }
                if(CommonUtils.isEmpty(list)){
                    IMUser imUser = list.get(0);
                    LogUtils.i("imUser:"+imUser.toString());
                    mSearchResultView.setVisibility(View.VISIBLE);
                    include_empty_view.setVisibility(View.GONE);


                    //每次你查询有数据的话则清空
                    mList.clear();

                    addTitle("查询结果");
                    addContent(imUser);
                    // 每次添加完数据,适配器去刷新一下
                    mAddFriendAdapter.notifyDataSetChanged();
                    //推荐
                    pushUser();
                }else {
                    //显示空数据
                    include_empty_view.setVisibility(View.VISIBLE);
                    mSearchResultView.setVisibility(View.GONE);
                }
            }

        });
    }

    /**
     * 推荐好友
     */
    private void pushUser() {
        // 查询所有的好友,取100个
        BMobManager.getInstance().queryAllUser(new FindListener<IMUser>() {
            @Override
            public void done(List<IMUser> list, BmobException e) {
                if (e==null) {
                    if (CommonUtils.isEmpty(list)) {
                        addTitle("推荐好友");
                        int num=(list.size()<=100)?list.size():100;
                        for (int i = 0; i < list.size(); i++) {
                            // 不能推荐给自己
                            String phoneNumber=BMobManager.getInstance().getUser().getMobilePhoneNumber();
                            LogUtils.e("phoneNumber:"+phoneNumber);
                            if (list.get(i).getMobilePhoneNumber().equals(phoneNumber)) {
                                continue;
                            }
                            addContent(list.get(i));
                        }
                        // 每次添加完数据,适配器去刷新一下
                        mAddFriendAdapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    /**
     * 添加头部
     * */
    private void addTitle(String title){
        AddFriendModel model = new AddFriendModel();
        model.setType(AddFriendAdapter.TYPE_TITLE);
        model.setTitle(title);
        mList.add(model);
    }

    /**
     * 添加内容
     * */
    private void addContent(IMUser imUser){
        AddFriendModel model = new AddFriendModel();
        model.setType(AddFriendAdapter.TYPE_CONTENT);
        model.setUserId(imUser.getObjectId());
        model.setPhoto(imUser.getPhoto());
        model.setSex(imUser.isSex());
        model.setNickName(imUser.getNickName());
        model.setAge(imUser.getAge());
        model.setDesc(imUser.getDesc());
        mList.add(model);
    }

}
