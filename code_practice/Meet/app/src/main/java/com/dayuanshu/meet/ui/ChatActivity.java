package com.dayuanshu.meet.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dayuanshu.framework.adapter.CommonAdapter;
import com.dayuanshu.framework.adapter.CommonViewHolder;
import com.dayuanshu.framework.base.BaseBackActivity;
import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.cloud.CloudManager;
import com.dayuanshu.framework.entity.Constants;
import com.dayuanshu.framework.envent.EventManager;
import com.dayuanshu.framework.envent.MessageEvent;
import com.dayuanshu.framework.gson.TextBean;
import com.dayuanshu.framework.helper.FileHelper;
import com.dayuanshu.framework.model.ChatModel;
import com.dayuanshu.framework.utils.CommonUtils;
import com.dayuanshu.framework.utils.LogUtils;
import com.dayuanshu.meet.R;
import com.google.gson.Gson;


import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Message;
import io.rong.message.ImageMessage;
import io.rong.message.TextMessage;

/**
 * 聊天界面
 */
public class ChatActivity extends BaseBackActivity implements View.OnClickListener {

    /**
     * 发送文本逻辑:
     * 1.跳转到ChatActivity
     * 2.实现我们的聊天列表,适配器
     * 3.加载我们的历史记录
     * 4.实时更新我们的聊天信息
     * 5.发送消息
     */

    /**
     * 发送图片逻辑:
     * 1.读取(相机和相册)
     * 2.发送图片消息
     * 3.完成适配器的UI
     * 4.完成Service的图片接收逻辑
     * 5.通知UI刷新
     */

    //左边的文本
    public static final int TYPE_LEFT_TEXT = 0;

    public static final int TYPE_LEFT_IMAGE = 1;
    public static final int TYPE_LEFT_LOCATION = 2;
    //右边的文本
    public static final int TYPE_RIGHT_TEXT = 3;

    public static final int TYPE_RIGHT_IMAGE = 4;
    public static final int TYPE_RIGHT_LOCATION = 5;


    /**
     * 跳转
     * */
    public static void startActivity(Context context,String userId,String userName,String userPhoto){
        Intent intent = new Intent(context,ChatActivity.class);
        intent.putExtra(Constants.INTENT_USER_ID,userId);
        intent.putExtra(Constants.INTENT_USER_NAME,userName);
        intent.putExtra(Constants.INTENT_USER_PHOTO,userPhoto);
        context.startActivity(intent);
    }

    //聊天记录
    private RecyclerView recyclerView;
    //输入框
    private EditText et_input_msg;
    //发送按钮
    private Button btn_send_msg;
    //语音输入
    private LinearLayout ll_voice;
    //相机
    private LinearLayout ll_camera;
    //图片
    private LinearLayout ll_pic;
    //位置
    private LinearLayout ll_location;

    //对方用户信息
    private String youUserId;
    private String youUserPhoto;
    private String youUserName;

    //自己的信息
    private String meUserPhoto;

    //列表
    private CommonAdapter<ChatModel> mChatAdapter;
    private List<ChatModel> mList = new ArrayList<>();

    //图片文件
    private File uploadFile =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        initView();
    }

    private void initView() {
        recyclerView = findViewById(R.id.mChatView);
        et_input_msg = findViewById(R.id.et_input_msg);
        btn_send_msg = findViewById(R.id.btn_send_msg);
        ll_voice = findViewById(R.id.ll_voice);
        ll_camera = findViewById(R.id.ll_camera);
        ll_pic = findViewById(R.id.ll_pic);
        ll_location = findViewById(R.id.ll_location);

        btn_send_msg.setOnClickListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mChatAdapter = new CommonAdapter<>(mList, new CommonAdapter.OnMoreBindDataListener<ChatModel>() {
            @Override
            public int getItemType(int position) {
                return mList.get(position).getType();
            }

            @Override
            public void onBindViewHolder(ChatModel model, CommonViewHolder viewHolder, int type, int position) {
                switch (model.getType()){
                    case TYPE_LEFT_TEXT:
                        viewHolder.setText(R.id.tv_left_text,model.getText());
                        viewHolder.setImageUrl(ChatActivity.this,R.id.iv_left_photo,youUserPhoto);
                        break;
                    case TYPE_RIGHT_TEXT:
                        viewHolder.setText(R.id.tv_right_text,model.getText());
                        viewHolder.setImageUrl(ChatActivity.this,R.id.iv_right_photo,meUserPhoto);
                        break;
                    case TYPE_LEFT_IMAGE:
                       /* viewHolder.setImageUrl(ChatActivity.this,R.id.iv_left_img,model.getImgUrl());
                        viewHolder.setImageUrl(ChatActivity.this,R.id.iv_left_photo,youUserPhoto);*/
                        break;
                    case TYPE_RIGHT_IMAGE:
                        /*if (TextUtils.isEmpty(model.getImgUrl())){
                            if (model.getLocalFile()!=null){
                                //加载本地文件
                                viewHolder.setImageFile(ChatActivity.this,R.id.iv_right_img,model.getLocalFile());
                            }
                        }else {
                            viewHolder.setImageUrl(ChatActivity.this,R.id.iv_right_img,model.getImgUrl());
                        }
                        viewHolder.setImageUrl(ChatActivity.this,R.id.iv_right_photo,meUserPhoto);*/
                        break;
                    case TYPE_LEFT_LOCATION:
                        break;
                    case TYPE_RIGHT_LOCATION:
                        break;
                }

            }

            @Override
            public int getLayoutId(int type) {
                if (type == TYPE_LEFT_TEXT){
                    return R.layout.layout_chat_left_text;
                }else if (type == TYPE_RIGHT_TEXT){
                    return R.layout.layout_chat_right_text;
                }else if (type == TYPE_LEFT_IMAGE){
                    return R.layout.layout_chat_left_img;
                }else if (type == TYPE_RIGHT_IMAGE){
                    return R.layout.layout_chat_right_img;
                }else if (type == TYPE_LEFT_LOCATION){
                    return R.layout.layout_chat_left_location;
                }else if (type == TYPE_RIGHT_LOCATION){
                    return R.layout.layout_chat_right_location;
                }
                return 0;
            }
        });
        recyclerView.setAdapter(mChatAdapter);

        loadMeInfo();

        queryMessages();

    }

    /**
     * 查询聊天记录
     * */
    private void queryMessages() {
        CloudManager.getInstance().getHistoryMessages(youUserId, new RongIMClient.ResultCallback<List<Message>>() {
            @Override
            public void onSuccess(List<Message> messages) {
                System.out.println("查询聊天记录。。。。。。。。。。。。。。");
                if (CommonUtils.isEmpty(messages)){
                    try {
                        parsingListMessage(messages);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    queryRemoteMessage();
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
                LogUtils.e("errorCode:"+errorCode);
            }
        });
    }

    /**
     * 查询服务器历史记录
     * */
    private void queryRemoteMessage() {
        CloudManager.getInstance().getRemoteHistoryMessages(youUserId, new RongIMClient.ResultCallback<List<Message>>() {
            @Override
            public void onSuccess(List<Message> messages) {
                if (CommonUtils.isEmpty(messages)){
                    try {
                        parsingListMessage(messages);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
                LogUtils.e("errorCode:"+errorCode);
            }
        });
    }

    /**
     * 解析历史记录
     * */
    private void parsingListMessage(List<Message> messages){
        //倒序
        Collections.reverse(messages);
        //遍历
        for (int i=0;i<messages.size();i++){
            Message m = messages.get(i);
            String objectName = m.getObjectName();
            if (objectName.equals(CloudManager.MSG_TEXT_NAME)){
                TextMessage textMessage = (TextMessage) m.getContent();
                String msg = textMessage.getContent();
                try {
                    TextBean textBean = new Gson().fromJson(msg, TextBean.class);
                    if (textBean.getType().equals(CloudManager.TYPE_TEXT)){
                        //添加到ui 判断你还是我
                        if (m.getSenderUserId().equals(youUserId)){
                            addText(0,textBean.getMsg());
                        }else {
                            addText(1,textBean.getMsg());
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else if (objectName.equals(CloudManager.MSG_IMAGE_NAME)){
                ImageMessage imageMessage = (ImageMessage) m.getContent();
                String  url = imageMessage.getRemoteUri().toString();
                if (!TextUtils.isEmpty(url)){
                    LogUtils.i("url: "+url);
                    if (m.getSenderUserId().equals(youUserId)){
                        //addImage(0,url);
                    }else {
                        //addImage(1,url);
                    }
                }

            }else if (objectName.equals(CloudManager.MSG_LOCATION_NAME)){

            }
        }
    }

    /**
     * 加载自我信息
     * */
    private void loadMeInfo() {
        Intent intent = getIntent();
        youUserId = intent.getStringExtra(Constants.INTENT_USER_ID);
        youUserPhoto = intent.getStringExtra(Constants.INTENT_USER_PHOTO);
        youUserName = intent.getStringExtra(Constants.INTENT_USER_NAME);

        meUserPhoto = BMobManager.getInstance().getUser().getPhoto();

        //设置标题
        if (!TextUtils.isEmpty(youUserName)){
            getSupportActionBar().setTitle(youUserName);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_send_msg:
                String inputText = et_input_msg.getText().toString().trim();
                if (TextUtils.isEmpty(inputText)){
                    return;
                }
                CloudManager.getInstance().sendTextMessage(inputText,CloudManager.TYPE_TEXT,youUserId);
                addText(1,inputText);
                //清空
                et_input_msg.setText("");
                break;
            case R.id.ll_voice:
                break;
            case R.id.ll_camera:
                //跳转到相机
                FileHelper.getInstance().toCamera(this);
                break;
            case R.id.ll_pic:
                //跳转到相册
                FileHelper.getInstance().toAlbum(this);
                break;
            case R.id.ll_location:
                break;
        }
    }

    /**
     * 添加数据的基类
     * */
    private void baseAddItem(ChatModel model){
        mList.add(model);
        mChatAdapter.notifyDataSetChanged();
        //滑动到底部
        recyclerView.scrollToPosition(mList.size() - 1);
    }

    /**
     * 添加左边的文字
     * 0:左边 1：右边
     * */
    private void addText(int index,String text){
        ChatModel model = new ChatModel();
        if (index == 0){
            model.setType(TYPE_LEFT_TEXT);
        }else {
            model.setType(TYPE_RIGHT_TEXT);
        }
        model.setText(text);
        baseAddItem(model);
    }

   /* @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (!event.getUserId().equals(youUserId)){
            return;
        }

        switch (event.getType()){
            case EventManager.FLAG_SEND_TEXT:
                addText(0,event.getText());
                break;
            case EventManager.FLAG_SEND_IMAGE:
                addImage(0,event.getImgUrl());
                break;
        }
    }*/

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode,Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FileHelper.CAMEAR_REQUEST_CODE) {
                uploadFile = FileHelper.getInstance().getTimeFile();
            } else if (requestCode == FileHelper.ALBUM_REQUEST_CODE) {
                Uri uri = data.getData();
                if (uri != null) {
                    String path = FileHelper.getInstance().getRealPathFromURI(this, uri);
                    if (!TextUtils.isEmpty(path)) {
                        uploadFile = new File(path);
                    }
                }
            }
            if (uploadFile != null){
                //发送图片消息
                CloudManager.getInstance().sendImageMessage(youUserId,uploadFile);
                //更新列表
                addImage(1,uploadFile);
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }*/

    /**
     * 添加图片
     * */
    /*private void addImage(int index,String url){
        ChatModel model = new ChatModel();
        if (index == 0){
            model.setType(TYPE_LEFT_IMAGE);
        }else {
            model.setType(TYPE_RIGHT_IMAGE);
        }
        model.setImgUrl(url);
        baseAddItem(model);
    }*/

    /**
     * 添加图片
     * */
    /*private void addImage(int index, File file){
        ChatModel model = new ChatModel();
        if (index == 0){
            model.setType(TYPE_LEFT_IMAGE);
        }else {
            model.setType(TYPE_RIGHT_IMAGE);
        }
        model.setLocalFile(file);
        baseAddItem(model);
    }*/

}
