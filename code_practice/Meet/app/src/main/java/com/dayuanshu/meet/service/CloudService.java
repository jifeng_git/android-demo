package com.dayuanshu.meet.service;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.cloud.CloudManager;
import com.dayuanshu.framework.db.LitePalHelper;
import com.dayuanshu.framework.db.NewFriend;
import com.dayuanshu.framework.entity.Constants;
import com.dayuanshu.framework.envent.EventManager;
import com.dayuanshu.framework.envent.MessageEvent;
import com.dayuanshu.framework.gson.TextBean;
import com.dayuanshu.framework.utils.CommonUtils;
import com.dayuanshu.framework.utils.LogUtils;
import com.dayuanshu.framework.utils.SpUtils;
import com.google.gson.Gson;

import java.util.List;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

/**
 * 云服务
 */
public class CloudService extends Service {

    private Disposable disposable;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        linkCloudServer();
    }

    /**
     * 连接云服务
     * */
    private void linkCloudServer() {
        //获取token
        String token = SpUtils.getInstance().getString(Constants.SP_TOKEN,"");
        LogUtils.e("token: " +token);
        //连接服务
        CloudManager.getInstance().connect(token);
        //接收消息
        CloudManager.getInstance().setOnReceiveMessageListener(new RongIMClient.OnReceiveMessageListener() {
            @Override
            public boolean onReceived(Message message, int i) {
                LogUtils.i("message:    "+message);
                String objectName = message.getObjectName();
                //文本消息
                if(objectName.equals(CloudManager.MSG_TEXT_NAME)){
                    //获取消息主体
                    TextMessage textMessage = (TextMessage) message.getContent();
                    String content = textMessage.getContent();
                    LogUtils.i("content:    "+content);
                    //
                    TextBean textBean = new Gson().fromJson(content,TextBean.class);
                    //普通消息
                    if(textBean.getType().equals(CloudManager.TYPE_TEXT)){
                        MessageEvent event = new MessageEvent(EventManager.FLAG_SEND_TEXT);
                        event.setText(textBean.getMsg());
                        event.setUserId(message.getSenderUserId());
                        EventManager.post(event);
                        //添加好友消息
                    }else if (textBean.getType().equals(CloudManager.TYPE_ADD_FRIEND)){
                        LogUtils.i("添加好友消息");
                        //查询数据库如果有重复的则不添加
                        disposable = Observable.create(new ObservableOnSubscribe<List<NewFriend>>() {
                                    @Override
                                    public void subscribe(ObservableEmitter<List<NewFriend>> emitter) throws Exception {
                                        emitter.onNext(LitePalHelper.getInstance().queryNewFriend());
                                        emitter.onComplete();
                                    }
                                }).subscribeOn(Schedulers.newThread())
                                .subscribeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Consumer<List<NewFriend>>() {
                                    @Override
                                    public void accept(List<NewFriend> newFriends) throws Exception {
                                        if(CommonUtils.isEmpty(newFriends)){
                                            boolean isHave = false;
                                            for (int i= 0;i<newFriends.size();i++){
                                                NewFriend friend =  newFriends.get(i);
                                                if(message.getSenderUserId().equals(friend.getUserId())){
                                                    isHave = true;
                                                    break;
                                                }
                                            }
                                            //防止重复添加
                                            if(!isHave){
                                                LogUtils.i("我来添加新盆友请求的数据了");
                                                LitePalHelper.getInstance().saveNewFriend(textBean.getMsg(),
                                                        message.getSenderUserId());
                                            }
                                        }else {
                                            LitePalHelper.getInstance().saveNewFriend(textBean.getMsg(),
                                                    message.getSenderUserId());
                                        }


                                    }
                                });

                        //同意添加好友消息
                    }else if (textBean.getType().equals(CloudManager.TYPE_AGREE_FRIEND)){
                        //1.添加到好友列表
                        BMobManager.getInstance().addFriendById(message.getSenderUserId(), new SaveListener<String>() {
                            @Override
                            public void done(String s, BmobException e) {
                                if(e == null){
                                    //2.刷新好友列表
                                    EventManager.post(EventManager.FLAG_UPDATE_FRIEND_LIST);
                                }
                            }
                        });
                    }
                }
                return false;
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (disposable.isDisposed()){
            disposable.dispose();
        }
    }
}
