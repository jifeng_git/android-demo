package com.dayuanshu.meet.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dayuanshu.framework.adapter.CommonAdapter;
import com.dayuanshu.framework.adapter.CommonViewHolder;
import com.dayuanshu.framework.base.BaseUIActivity;
import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.bmob.Friend;
import com.dayuanshu.framework.bmob.IMUser;
import com.dayuanshu.framework.cloud.CloudManager;
import com.dayuanshu.framework.entity.Constants;
import com.dayuanshu.framework.helper.GlideHelper;
import com.dayuanshu.framework.manager.DialogManager;
import com.dayuanshu.framework.model.UserInfoModel;
import com.dayuanshu.framework.utils.CommonUtils;
import com.dayuanshu.framework.view.DialogView;
import com.dayuanshu.meet.R;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * 用户信息
 * 1.根据传递过来的ID,查询用户信息 并且显示
 *   --普通信息
 *   --构建一个RecyclerView 宫格
 * 2.建立好友关系模型
 *  与我有关系的是好友
 *  1.在我的好友列表中
 *  2.同意了我的好友申请 BmobObject 建表
 *  3.查询所有的Friend表,其中user对应的列都是我的好友
 * 3.实现添加好友提示框
 * 4.发送添加好友的消息
 *  1.自定义消息类型
 *  2.自定义协议
 *  发送文本消息  Content,我们对文本进行处理:增加Json 定义一个标记来显示
 *  点击提示框的发送按钮去发送
 * 5.接受好友的消息
 */
public class UserInfoActivity extends BaseUIActivity implements View.OnClickListener {

    /**
     * 跳转
     * @param mContext
     * @param userId
     */
    public static void startActivity(Context mContext,String userId){
        Intent intent = new Intent(mContext, UserInfoActivity.class);
        intent.putExtra(Constants.INTENT_USER_ID,userId);
        mContext.startActivity(intent);
    }

    private ImageView iv_back;
    private CircleImageView iv_uer_photo;
    private TextView tv_nickname;
    private TextView tv_desc;
    // 用户信息
    private RecyclerView mUserInfoView;
    private CommonAdapter<UserInfoModel> mUserInfoAdapter;
    private List<UserInfoModel> mUserInfoList=new ArrayList<>();

    private Button btn_add_friend;
    private Button btn_chat;
    private Button btn_audio_chat;
    private Button btn_video_chat;

    private LinearLayout ll_is_friend;

    // 用户Id
    private String userId="";
    private IMUser imUser;

    // 添加好友提示框
    private DialogView mAddFriendDialogView;
    private EditText et_msg;
    private TextView tv_cancel;
    private TextView tv_add_friend;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        initView();
    }

    /**
     * 初始化View
     */
    private void initView() {
        // 添加好友对话框
        initAddFriendDialog();
        // 获取用户Id
        userId= getIntent().getStringExtra(Constants.INTENT_USER_ID);

        iv_back=findViewById(R.id.iv_back);
        iv_uer_photo=findViewById(R.id.iv_user_photo);
        tv_nickname=findViewById(R.id.tv_nickname);
        tv_desc=findViewById(R.id.tv_desc);
        mUserInfoView=findViewById(R.id.mUserInfoView);
        btn_add_friend=findViewById(R.id.btn_add_friend);
        btn_chat=findViewById(R.id.btn_chat);
        btn_audio_chat=findViewById(R.id.btn_audio_chat);
        btn_video_chat=findViewById(R.id.btn_video_chat);
        ll_is_friend=findViewById(R.id.ll_is_friend);

        // 设置点击事件
        iv_back.setOnClickListener(this);
        btn_add_friend.setOnClickListener(this);
        btn_chat.setOnClickListener(this);
        btn_audio_chat.setOnClickListener(this);
        btn_video_chat.setOnClickListener(this);

        // 列表
        mUserInfoAdapter=new CommonAdapter<>(mUserInfoList, new CommonAdapter.OnBindDataListener<UserInfoModel>() {
            @Override
            public void onBindViewHolder(UserInfoModel model, CommonViewHolder viewHolder, int type, int position) {
                // 添加背景颜色
                //viewHolder.setBackgroundColor(R.id.ll_bg,model.getBgColor());
                viewHolder.getView(R.id.ll_bg).setBackgroundColor(model.getBgColor());
                viewHolder.setText(R.id.tv_type,model.getTitle());
                viewHolder.setText(R.id.tv_content,model.getContent());
            }

            @Override
            public int getLayoutId(int type) {
                return R.layout.layout_user_info_item;
            }
        });
        mUserInfoView.setLayoutManager(new GridLayoutManager(this,3));
        mUserInfoView.setAdapter(mUserInfoAdapter);
        // 查询用户信息
        queryUserInfo();
    }

    /**
     * 添加好友提示框
     */
    private void initAddFriendDialog() {
        mAddFriendDialogView = DialogManager.getInstance().initView(this, R.layout.dialog_send_friend);
        et_msg=mAddFriendDialogView.findViewById(R.id.et_msg);
        tv_cancel=mAddFriendDialogView.findViewById(R.id.tv_cancel);
        tv_add_friend=mAddFriendDialogView.findViewById(R.id.tv_add_friend);

        // 添加好友点击事件
        tv_cancel.setOnClickListener(this);
        tv_add_friend.setOnClickListener(this);
    }

    /**
     * 查询用户信息
     */
    private void queryUserInfo() {
        if (TextUtils.isEmpty(userId)) {
            return;
        }
        // 查询用户信息
        BMobManager.getInstance().queryObjectIdUser(userId, new FindListener<IMUser>() {
            @Override
            public void done(List<IMUser> list, BmobException e) {
                if (e==null) {
                    if (CommonUtils.isEmpty(list)) {
                       imUser = list.get(0);
                        updateUserInfo(imUser);
                    }
                }
            }
        });
        // 判断好友关系
        BMobManager.getInstance().queryMyFriends(new FindListener<Friend>() {
            @Override
            public void done(List<Friend> list, BmobException e) {
                if (e==null) {
                    if (CommonUtils.isEmpty(list)) {
                        // 有好友
                        for (int i = 0; i < list.size(); i++) {
                            Friend friend = list.get(0);
                            // 判断这个对象中的id是否跟我目前的userId相同
                            if (friend.getFriendUser().getObjectId().equals(userId)) {
                                // 你们是好友关系
                                btn_add_friend.setVisibility(View.GONE);
                                ll_is_friend.setVisibility(View.VISIBLE);
                            }
                        }
                    }else {
                        System.out.println("好友列表为空.........");
                    }
                }
            }
        });
    }

    /**
     * 更新用户信息
     * @param imUser
     */
    private void updateUserInfo(IMUser imUser) {
        // 设置基本属性
        //1.头像
        GlideHelper.loadUrl(UserInfoActivity.this,imUser.getPhoto(),iv_uer_photo);
        //2.昵称
        tv_nickname.setText(imUser.getNickName());
        // 描述
        tv_desc.setText(imUser.getDesc());
        // 性别 年龄 生日 星座 爱好 单身状态
        addUserInfoModel(0x88F068F,getString(R.string.text_me_info_sex),imUser.isSex()?getString(R.string.text_me_info_boy):getString(R.string.text_me_info_girl));
        addUserInfoModel(0x88FF69B4,getString(R.string.text_me_info_age),imUser.getAge()+getString(R.string.text_search_age));
        addUserInfoModel(0x88CDAA7D,getString(R.string.text_me_info_birthday),imUser.getBirthday());
        addUserInfoModel(0x889AFF9A,getString(R.string.text_me_info_constellation),imUser.getConstellation());
        addUserInfoModel(0x887EC0EE,getString(R.string.text_me_info_hobby),imUser.getHobby());
        addUserInfoModel(0x8800FA9A,getString(R.string.text_me_info_status),imUser.getStatus());

        // 刷新数据
        mUserInfoAdapter.notifyDataSetChanged();

    }

    /**
     * 添加数据
     * @param color
     * @param title
     * @param content
     */
    private void addUserInfoModel(int color,String title,String content){
        UserInfoModel userInfoModel = new UserInfoModel();
        userInfoModel.setBgColor(color);
        userInfoModel.setTitle(title);
        userInfoModel.setContent(content);
        mUserInfoList.add(userInfoModel);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_add_friend:
                // 点击发送
                String msg = et_msg.getText().toString().trim();
                if (TextUtils.isEmpty(msg)) {
                    msg="你好,我是"+BMobManager.getInstance().getUser().getNickName();
                }
                CloudManager.getInstance().sendTextMessage(msg,CloudManager.TYPE_ADD_FRIEND,userId);
                DialogManager.getInstance().hide(mAddFriendDialogView);
                Toast.makeText(this,"消息发送成功",Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_cancel:
                DialogManager.getInstance().hide(mAddFriendDialogView);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_add_friend:
                // 添加好友
                DialogManager.getInstance().show(mAddFriendDialogView);
                break;
            case R.id.btn_chat:
                // 聊天界面
                ChatActivity.startActivity(UserInfoActivity.this,userId,imUser.getNickName(),imUser.getPhoto());
                break;
            case R.id.btn_audio_chat:
                break;
            case R.id.btn_video_chat:
                break;

        }
    }
}
