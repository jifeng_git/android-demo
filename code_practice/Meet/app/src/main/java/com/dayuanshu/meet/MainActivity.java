package com.dayuanshu.meet;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.dayuanshu.framework.base.BaseUIActivity;
import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.entity.Constants;
import com.dayuanshu.framework.gson.TokenBean;
import com.dayuanshu.framework.manager.DialogManager;
import com.dayuanshu.framework.manager.HttpManager;
import com.dayuanshu.framework.utils.LogUtils;
import com.dayuanshu.framework.utils.SpUtils;
import com.dayuanshu.framework.view.DialogView;
import com.dayuanshu.meet.fragment.ChatFragment;
import com.dayuanshu.meet.fragment.MeFragment;
import com.dayuanshu.meet.fragment.SquareFragment;
import com.dayuanshu.meet.fragment.StarFragment;
import com.dayuanshu.meet.service.CloudService;
import com.dayuanshu.meet.ui.FirstUploadActivity;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends BaseUIActivity implements View.OnClickListener {

    // 星球
    private ImageView iv_star;
    private TextView tv_star;
    private LinearLayout ll_star;
    private StarFragment mStarFragment=null;
    private FragmentTransaction mStarTransaction=null;

    // 广场
    private ImageView iv_square;
    private TextView tv_square;
    private LinearLayout ll_square;
    private SquareFragment mSquareFragment=null;
    private FragmentTransaction mSquareTransaction=null;

    // 聊天
    private ImageView iv_chat;
    private TextView tv_chat;
    private LinearLayout ll_chat;
    private ChatFragment mChatFragment=null;
    private FragmentTransaction mChatTransaction=null;

    // 我的
    private ImageView iv_me;
    private TextView tv_me;
    private LinearLayout ll_me;
    private MeFragment mMeFragment=null;
    private FragmentTransaction mMeTransaction=null;

    /**
     * 申请运行时权限的
     */
    private static final int PERMISSION_REQUEST_CODE=1000;

    // 跳转上传头像的回调
    private static final int UPLOAD_PEQUEST_CODE=1002;

    private Disposable disposable;

    /**
     * 1.初始化Fragment
     * 2.显示Fragment
     * 3.隐藏所有的Fragment
     * 4.恢复Fragment
     * 优化的手段
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    /**
     * 初始化视图
     */
    private void initView() {
        // 动态权限
        requestPermiss();

        //星球
        iv_star=findViewById(R.id.iv_star);
        tv_star=findViewById(R.id.tv_star);
        ll_star=findViewById(R.id.ll_star);

        // 广场
        iv_square=findViewById(R.id.iv_square);
        tv_square=findViewById(R.id.tv_square);
        ll_square=findViewById(R.id.ll_square);

        // 聊天
        iv_chat=findViewById(R.id.iv_chat);
        tv_chat=findViewById(R.id.tv_chat);
        ll_chat=findViewById(R.id.ll_chat);

        // 我
        iv_me=findViewById(R.id.iv_me);
        tv_me=findViewById(R.id.tv_me);
        ll_me=findViewById(R.id.ll_me);

        // 设置对应的点击事件
        ll_star.setOnClickListener(this);
        ll_square.setOnClickListener(this);
        ll_chat.setOnClickListener(this);
        ll_me.setOnClickListener(this);

        // 设置文本
        tv_star.setText(getString(R.string.text_main_star));
        tv_square.setText(getString(R.string.text_main_square));
        tv_chat.setText(getString(R.string.text_main_chat));
        tv_me.setText(getString(R.string.text_main_me));
        // 初始化Fragment
        initFragment();
        // 切换默认的选项卡
        checkMainTab(0);
        // 检查token
        checkToken();
        // 模拟数据
        //SimulationData.testData();
    }

    /**
     * 检查token
     * 获取token需要三个参数:
     * 1.用户id
     * 2.头像地址
     * 3.昵称
     */
    private void checkToken() {
        System.out.println("检查token的方法.....");
        String token = SpUtils.getInstance().getString(Constants.SP_TOKEN, "");
        if (!TextUtils.isEmpty(token)) {
            System.out.println("token已经存在......");
            startCloudService();
            // 启动云服务去连接融云服务
            //startActivity(new Intent(this, CloudService.class));
        }else {
            System.out.println("检查token,token不存在.......");
            // 存在着两个参数
            String tokenPhoto = BMobManager.getInstance().getUser().getTokenPhoto();
            String tokenNickName = BMobManager.getInstance().getUser().getTokenNickName();
            if (!TextUtils.isEmpty(tokenPhoto) && !TextUtils.isEmpty(tokenNickName)) {
                // 创建token
                createToken();
            }else {
                    // 创建上传提示框
                createUploadDialog();
            }
        }
    }

    /**
     * 创建上传提示框
     */
    private void createUploadDialog() {
        final DialogView mUploadDialogView = DialogManager.getInstance().initView(this, R.layout.dialog_first_upload);
       // 外部点击不能消失
        mUploadDialogView.setCancelable(false);
        ImageView iv_go_upload=mUploadDialogView.findViewById(R.id.iv_go_upload);
        iv_go_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.getInstance().hide(mUploadDialogView);
                FirstUploadActivity.startActivity(MainActivity.this);
            }
        });
        // 显示出来
        DialogManager.getInstance().show(mUploadDialogView);
    }

    /**
     * 创建Token
     */
    private void createToken() {
        LogUtils.i("createToken");
        // 判断是否出现登录异常
        if(BMobManager.getInstance().getUser() == null){
            Toast.makeText(this, "登录异常", Toast.LENGTH_SHORT).show();
            return;
        }

        /**
         * 1.去融云后台获取token
         * 2.连接融云
         */
        final HashMap<String, String> map = new HashMap<>();
        map.put("userId",BMobManager.getInstance().getUser().getObjectId());
        map.put("name",BMobManager.getInstance().getUser().getTokenNickName());
        map.put("portraitUri",BMobManager.getInstance().getUser().getTokenPhoto());

        // 通过Okhttp去请求token
        //线程调度
        /*disposable = Observable.create((ObservableOnSubscribe<String>) emitter -> {
                    //执行请求过程
                    String json = HttpManager.getInstance().postCloudToken(map);
                    LogUtils.i("json:" + json);
                    emitter.onNext(json);
                    emitter.onComplete();
                }).subscribeOn(Schedulers.newThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> parsingCloudToken(s));*/
        disposable=Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> emitter) throws Exception{
                //执行请求过程
                String json = HttpManager.getInstance().postCloudToken(map);
                LogUtils.i("json:" + json);
                emitter.onNext(json);
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.newThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        // 解析token
                        System.out.println("成功了......");
                        parsingCloudToken(s);
                        LogUtils.e("获取到的值s:"+s);
                    }
                });
    }

    /**
     * 销毁
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 判断资源是否被释放
        if (disposable.isDisposed()) {
            // 销毁
            disposable.dispose();
        }
    }

    /**
     * 解析token
     * @param s
     */
    private void parsingCloudToken(String s) {
        try {
            LogUtils.e("parsingCloudToken:" + s);
            TokenBean tokenBean = new Gson().fromJson(s, TokenBean.class);
            if (tokenBean.getCode() == 200) {
                if (!TextUtils.isEmpty(tokenBean.getToken())) {
                    //保存Token
                    SpUtils.getInstance().putString(Constants.SP_TOKEN, tokenBean.getToken());
                    // 启动云服务去连接云服务
                    startCloudService();
                }
            } else if (tokenBean.getCode() == 2007) {
                Toast.makeText(this, "注册人数已达上限，请替换成自己的Key", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            LogUtils.i("parsingCloudToken:" + e.toString());
        }
    }

    /**
     * 启动云服务去连接融云服务
     */
    private void startCloudService() {
        LogUtils.i("startCloudService");
        startService(new Intent(this, CloudService.class));
        //startActivity(new Intent(this, CloudService.class));
        //检查更新
        //new UpdateHelper(this).updateApp(null);
    }

    /**
     * 初始化Fragment
     */
    private void initFragment() {
        // 星球
        if (mStarFragment==null) {
            mStarFragment=new StarFragment();
            mStarTransaction= getSupportFragmentManager().beginTransaction();
            mStarTransaction.add(R.id.mMainLayout,mStarFragment);
            mStarTransaction.commit();
        }
        // 广场
        if (mSquareFragment==null) {
            mSquareFragment=new SquareFragment();
            mSquareTransaction= getSupportFragmentManager().beginTransaction();
            mSquareTransaction.add(R.id.mMainLayout,mSquareFragment);
            mSquareTransaction.commit();
        }
        // 聊天
        if (mChatFragment==null) {
            mChatFragment=new ChatFragment();
            mChatTransaction= getSupportFragmentManager().beginTransaction();
            mChatTransaction.add(R.id.mMainLayout,mChatFragment);
            mChatTransaction.commit();
        }
        // 我的
        if (mMeFragment==null) {
            mMeFragment=new MeFragment();
            mMeTransaction= getSupportFragmentManager().beginTransaction();
            mMeTransaction.add(R.id.mMainLayout,mMeFragment);
            mMeTransaction.commit();
        }
    }

    /**
     * 显示Fragment
     * @param fragment
     */
    private void showFragment(Fragment fragment){
        if (fragment!=null) {
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            // 显示之前先隐藏
            hideAllFragment(transaction);
            transaction.show(fragment);
            transaction.commitAllowingStateLoss();
        }
    }

    /**
     * 隐藏所有的Fragment
     * @param transaction
     */
    private void hideAllFragment(FragmentTransaction transaction){
        if (mStarFragment!=null) {
            transaction.hide(mStarFragment);
        }
        if (mSquareFragment!=null) {
            transaction.hide(mSquareFragment);
        }
        if (mChatFragment!=null) {
            transaction.hide(mChatFragment);
        }
        if (mMeFragment!=null) {
            transaction.hide(mMeFragment);
        }
    }

    /**
     * 切换首页选项卡
     * 0:星球
     * 1:广场
     * 2:聊天
     * 3:我的
     * @param index
     */
    private void checkMainTab(int index){
        switch (index){
            case 0:
                showFragment(mStarFragment);
                // 显示图片和文本的颜色
                iv_star.setImageResource(R.drawable.img_star_p);
                iv_square.setImageResource(R.drawable.img_square);
                iv_chat.setImageResource(R.drawable.img_chat);
                iv_me.setImageResource(R.drawable.img_me);

                // 更改对应的颜色
                tv_star.setTextColor(getResources().getColor(R.color.colorAccent));
                tv_square.setTextColor(Color.BLACK);
                tv_chat.setTextColor(Color.BLACK);
                tv_me.setTextColor(Color.BLACK);
                break;
            case 1:
                showFragment(mSquareFragment);
                // 显示图片和文本的颜色
                iv_star.setImageResource(R.drawable.img_star);
                iv_square.setImageResource(R.drawable.img_square_p);
                iv_chat.setImageResource(R.drawable.img_chat);
                iv_me.setImageResource(R.drawable.img_me);
                // 更改对应的颜色
                tv_star.setTextColor(Color.BLACK);
                tv_square.setTextColor(getResources().getColor(R.color.colorAccent));
                tv_chat.setTextColor(Color.BLACK);
                tv_me.setTextColor(Color.BLACK);
                break;
            case 2:
                showFragment(mChatFragment);
                // 显示图片和文本的颜色
                iv_star.setImageResource(R.drawable.img_star);
                iv_square.setImageResource(R.drawable.img_square);
                iv_chat.setImageResource(R.drawable.img_chat_p);
                iv_me.setImageResource(R.drawable.img_me);
                // 更改对应的颜色
                tv_star.setTextColor(Color.BLACK);
                tv_square.setTextColor(Color.BLACK);
                tv_chat.setTextColor(getResources().getColor(R.color.colorAccent));
                tv_me.setTextColor(Color.BLACK);
                break;
            case 3:
                showFragment(mMeFragment);
                // 显示图片和文本的颜色
                iv_star.setImageResource(R.drawable.img_star);
                iv_square.setImageResource(R.drawable.img_square);
                iv_chat.setImageResource(R.drawable.img_chat);
                iv_me.setImageResource(R.drawable.img_me_p);
                // 更改对应的颜色
                tv_star.setTextColor(Color.BLACK);
                tv_square.setTextColor(Color.BLACK);
                tv_chat.setTextColor(Color.BLACK);
                tv_me.setTextColor(getResources().getColor(R.color.colorAccent));
                break;
        }
    }

    /**
     * 防止重叠
     * 当应用的内存紧张的时候,系统会回收掉Fragment对象
     * 再一次进入的时候,会重新创建Fragment
     * 非原来的对象,我们无法控制，大致重叠
     * @param fragment
     */
    @Override
    public void onAttachFragment(Fragment fragment) {
        // 星球
        if (mStarFragment!=null && fragment instanceof StarFragment) {
            mStarFragment=(StarFragment) fragment;
        }
        // 广场
        if (mSquareFragment!=null && fragment instanceof SquareFragment) {
            mSquareFragment=(SquareFragment) fragment;
        }
        // 聊天
        if (mChatFragment!=null && fragment instanceof ChatFragment) {
            mChatFragment=(ChatFragment) fragment;
        }
        // 我的
        if (mMeFragment!=null && fragment instanceof MeFragment) {
            mMeFragment=(MeFragment) fragment;
        }
    }

    /**
     * Fragment对应的点击事件
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_star:
                checkMainTab(0);
                break;
            case R.id.ll_square:
                checkMainTab(1);
                break;
            case R.id.ll_chat:
                checkMainTab(2);
                break;
            case R.id.ll_me:
                checkMainTab(3);
                break;
        }
    }

    /**
     * 请求权限
     */
    private void requestPermiss() {
        //危险权限
        request(new OnPermissionsResult() {
            @Override
            public void OnSuccess() {

            }

            @Override
            public void OnFail(List<String> noPermissions) {
                LogUtils.i("noPermissions:" + noPermissions.toString());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode== Activity.RESULT_OK) {
            if (resultCode==UPLOAD_PEQUEST_CODE) {
                // 说明头像上传成功了
                checkToken();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}