package com.dayuanshu.meet.ui;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dayuanshu.framework.adapter.CommonAdapter;
import com.dayuanshu.framework.adapter.CommonViewHolder;
import com.dayuanshu.framework.base.BaseBackActivity;
import com.dayuanshu.framework.bmob.BMobManager;
import com.dayuanshu.framework.bmob.IMUser;
import com.dayuanshu.framework.cloud.CloudManager;
import com.dayuanshu.framework.db.LitePalHelper;
import com.dayuanshu.framework.db.NewFriend;
import com.dayuanshu.framework.envent.EventManager;
import com.dayuanshu.framework.utils.CommonUtils;
import com.dayuanshu.framework.utils.LogUtils;
import com.dayuanshu.meet.R;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 新朋友
 * 1.查询好友的申请列表
 * 2.通过适配器现实出来
 * 3.如果同意则添加对方为自己的好友
 * 4.并且发送给对方自定义的消息
 * 5.对方将我添加到好友列表
 */
public class NewFriendActivity extends BaseBackActivity {

    private View item_empty_view;
    private RecyclerView mNewFriendView;
    private Disposable disposable;
    private CommonAdapter<NewFriend> mNewFriendAdapter;
    private List<NewFriend> mList=new ArrayList<>();
    // 对方用户
    private IMUser imUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_friend);
        initView();
    }

    /**
     * 初始化View
     */
    private void initView() {
        item_empty_view=findViewById(R.id.item_empty_view);
        mNewFriendView=findViewById(R.id.mNewFriendView);

        // 2.通过适配器实现出来
        mNewFriendView.setLayoutManager(new LinearLayoutManager(this));
        // 分割线
        mNewFriendView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        mNewFriendAdapter=new CommonAdapter<>(mList, new CommonAdapter.OnBindDataListener<NewFriend>() {
            @Override
            public void onBindViewHolder(NewFriend model, CommonViewHolder viewHolder, int type, int position) {
                // 根据id查询用户信息
                BMobManager.getInstance().queryObjectIdUser(model.getUserId(), new FindListener<IMUser>() {
                    @Override
                    public void done(List<IMUser> list, BmobException e) {
                        // 填充具体属性
                        if (e==null) {
                            LogUtils.e("没有任何异常出现.....");
                            System.out.println("没有任何异常出现...............");
                            imUser = list.get(0);
                            viewHolder.setImageUrl(NewFriendActivity.this,R.id.iv_photo,imUser.getPhoto());
                            viewHolder.setImageResource(R.id.iv_sex,imUser.isSex()?R.drawable.img_boy_icon:R.drawable.img_girl_icon);
                            viewHolder.setText(R.id.tv_nickname,imUser.getNickName());
                            viewHolder.setText(R.id.tv_age,imUser.getAge()+getString(R.string.text_search_age));
                            viewHolder.setText(R.id.tv_desc,imUser.getDesc());
                            viewHolder.setText(R.id.tv_msg,model.getMsg());
                            if (model.getIsAgree()==0) {
                                viewHolder.getView(R.id.ll_agree).setVisibility(View.GONE);
                                viewHolder.getView(R.id.tv_result).setVisibility(View.VISIBLE);
                                viewHolder.setText(R.id.tv_result,"已同意");
                            }else if(model.getIsAgree()==1){
                                viewHolder.getView(R.id.ll_agree).setVisibility(View.GONE);
                                viewHolder.getView(R.id.tv_result).setVisibility(View.VISIBLE);
                                viewHolder.setText(R.id.tv_result,"已拒绝");
                            }
                        }else {
                            LogUtils.e("查询新朋友抛异常了:"+e.toString());
                        }
                    }
                });

                // 同意
                viewHolder.getView(R.id.ll_yes).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /**
                         * 1.同意则刷新当前的Item
                         * 2.将好友添加到自己的好友列表
                         * 3.通知对方我已经同意了
                         * 4.对方将我添加好好友列表
                         * 5.刷新好友列表
                         */
                        updateItem(position,0);
                        // 2.将好友添加到自己的好友列表
                        BMobManager.getInstance().addFriend(imUser, new SaveListener<String>() {
                            @Override
                            public void done(String s, BmobException e) {
                                if (e==null) {
                                    // 保存成功,通知对方我已经同意了
                                    CloudManager.getInstance().sendTextMessage("",CloudManager.TYPE_AGREE_FRIEND,imUser.getObjectId());
                                    // 刷新好友列表
                                    EventManager.post(EventManager.FLAG_UPDATE_FRIEND_LIST);
                                }
                            }
                        });
                    }
                });
                // 拒绝
                viewHolder.getView(R.id.ll_no).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateItem(position,1);
                    }
                });

            }

            @Override
            public int getLayoutId(int type) {
                return R.layout.layout_new_friend_item;
            }
        });
        mNewFriendView.setAdapter(mNewFriendAdapter);
        // 1.查询新朋友
        queryNewFriends();
    }

    /**
     * 更新Item
     * @param position
     * @param i
     */
    private void updateItem(int position, int i) {
        NewFriend newFriend = mList.get(position);
        // 更新数据库
        LitePalHelper.getInstance().updateNewFriend(newFriend.getUserId(),i);
        //更新本地数据源
        newFriend.setIsAgree(i);
        mList.set(position,newFriend);
        mNewFriendAdapter.notifyDataSetChanged();
    }

    /**
     * 查询新朋友
     * 在子线程中获取好友申请列表然后在主线程中更新我们的UI
     * RxJava线程调度
     */
    private void queryNewFriends(){
       disposable= Observable.create(new ObservableOnSubscribe<List<NewFriend>>() {
            @Override
            public void subscribe(ObservableEmitter<List<NewFriend>> emitter) throws Exception{
                emitter.onNext(LitePalHelper.getInstance().queryNewFriend());
                emitter.onComplete();
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<NewFriend>>() {
                    @Override
                    public void accept(List<NewFriend> newFriends) throws Exception {
                        System.out.println("我来更新UI了");
                        // 更新UI
                        if (CommonUtils.isEmpty(newFriends)) {
                            mList.addAll(newFriends);
                            System.out.println("添加新朋友成功......");
                            mNewFriendAdapter.notifyDataSetChanged();
                        }else {
                            System.out.println("添加新朋友失败了......"+newFriends);
                            item_empty_view.setVisibility(View.VISIBLE);
                            mNewFriendView.setVisibility(View.GONE);
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
