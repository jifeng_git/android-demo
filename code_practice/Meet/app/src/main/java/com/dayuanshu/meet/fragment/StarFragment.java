package com.dayuanshu.meet.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dayuanshu.framework.base.BaseFragment;
import com.dayuanshu.meet.R;
import com.dayuanshu.meet.adapter.CloudTagAdapter;
import com.dayuanshu.meet.ui.AddFriendActivity;
import com.moxun.tagcloudlib.view.TagCloudView;

import java.util.ArrayList;
import java.util.List;

/**
 * 星球
 */
public class StarFragment extends BaseFragment implements View.OnClickListener {
    private Context mContext;
    // 扫码
    private ImageView iv_camera;
    // 添加好友
    private ImageView iv_add;
    // 3D星球
    private TagCloudView mCloudView;

    // 随机匹配
    private LinearLayout ll_random;
    // 灵魂匹配
    private LinearLayout ll_soul;
    // 缘分匹配
    private LinearLayout ll_fate;
    // 恋爱匹配
    private LinearLayout ll_love;

    private List<String> mStarList=new ArrayList<>();
    // 声明适配器
    private CloudTagAdapter mCloudTagAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_star,null);
        initView(view);
        return view;
    }

    /**
     * 初始化View
     * @param view
     */
    private void initView(View view){
        // 扫描和添加好友
        iv_camera=view.findViewById(R.id.iv_camera);
        iv_add = view.findViewById(R.id.iv_add);

        mCloudView=view.findViewById(R.id.mCloudView);
        // 随机匹配,灵魂匹配,缘分匹配,恋爱匹配
        ll_random=view.findViewById(R.id.ll_random);
        ll_soul=view.findViewById(R.id.ll_soul);
        ll_fate=view.findViewById(R.id.ll_fate);
        ll_love=view.findViewById(R.id.ll_love);

        // 对应的点击事件
        iv_camera.setOnClickListener(this);
        iv_add.setOnClickListener(this);

        ll_random.setOnClickListener(this);
        ll_soul.setOnClickListener(this);
        ll_fate.setOnClickListener(this);
        ll_love.setOnClickListener(this);

        for (int i = 0; i < 100; i++) {
            mStarList.add("Star"+i);
        }
        // 数据绑定
        mCloudTagAdapter=new CloudTagAdapter(getActivity(),mStarList);

        mCloudView.setAdapter(mCloudTagAdapter);

        // 监听点击事件
        mCloudView.setOnTagClickListener(new TagCloudView.OnTagClickListener(){
            @Override
            public void onItemClick(ViewGroup parent,View view,int position){
                Toast.makeText(getActivity(),"position:"+position,Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_camera:
                break;
            case R.id.iv_add:
                // 添加好友
                startActivity(new Intent(getActivity(),AddFriendActivity.class));
                break;
            case R.id.ll_random:
                //随机匹配
                break;
            case R.id.ll_soul:
                // 灵魂匹配
                break;
            case R.id.ll_fate:
                // 缘分匹配
                break;
            case R.id.ll_love:
                // 恋爱匹配
                break;
        }
    }
}
