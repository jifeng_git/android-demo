package com.dayuanshu.meet.test;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.dayuanshu.framework.base.BaseActivity;
import com.dayuanshu.framework.bmob.MyData;
import com.dayuanshu.framework.utils.LogUtils;
import com.dayuanshu.framework.view.TouchPictureV;
import com.dayuanshu.meet.R;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * 测试专用类
 */
public class TestActivity extends BaseActivity implements View.OnClickListener {

    // 四个按钮
    private Button btn_add;
    private Button btn_del;
    private Button btn_query;
    private Button btn_update;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        initView();
    }

    private void initView() {
        // 获取四个按钮
        btn_add=findViewById(R.id.btn_add);
        btn_del=findViewById(R.id.btn_del);
        btn_query=findViewById(R.id.btn_query);
        btn_update=findViewById(R.id.btn_update);

        // 四个按钮设置响应的点击事件
        btn_add.setOnClickListener(this);
        btn_del.setOnClickListener(this);
        btn_query.setOnClickListener(this);
        btn_update.setOnClickListener(this);

        // 获取滑块控件
       /* TouchPictureV touchPictureV = findViewById(R.id.touchPictureV);
        touchPictureV.setViewResultListener(new TouchPictureV.OnViewResultListener() {
            @Override
            public void onResult() {
            //Toast.makeText(this,"",Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    /**
     * 四个按钮的点击事件
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add:
                MyData myData = new MyData();
                myData.setName("张胜男");
                myData.setSex(0);
                // 添加
                myData.save(new SaveListener<String>() {
                    @Override
                    public void done(String s, BmobException e) {
                        if(e==null){
                            // 733cc73cb7
                            Toast.makeText(TestActivity.this,"新增成功"+s,Toast.LENGTH_SHORT).show();
                            LogUtils.e("新增成功:"+s);
                        }
                    }
                });
                break;
            case R.id.btn_del:
                MyData myData1 = new MyData();
                myData1.setObjectId("733cc73cb7");
                myData1.delete(new UpdateListener() {
                    @Override
                    public void done(BmobException e) {
                        if(e==null){
                            Toast.makeText(TestActivity.this,"删除成功",Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(TestActivity.this,"删除失败",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            case R.id.btn_query:
                // 1.查找MyData表里面id为704147f0c9的数据
                BmobQuery<MyData> bmobQuery=new BmobQuery<>();
                /*bmobQuery.getObject("704147f0c9", new QueryListener<MyData>() {
                    @Override
                    public void done(MyData myData, BmobException e) {
                        if(e==null){
                            Toast.makeText(TestActivity.this,"查询成功",Toast.LENGTH_SHORT).show();
                            LogUtils.e("MyData:name:"+myData.getName()+"<==>Sex:"+myData.getSex());
                        }else {
                            LogUtils.e("异常:"+e.toString());
                        }
                    }
                });*/
                // 2.条件查询
                //bmobQuery.addWhereEqualTo("name","张胜男");
                // 3.全部查询
                bmobQuery.findObjects(new FindListener<MyData>() {
                    @Override
                    public void done(List<MyData> list, BmobException e) {
                        if(e==null){
                            if(list!=null && list.size()>0){
                                for (int i = 0; i < list.size(); i++) {
                                    LogUtils.i(list.get(i).getName()+"="+list.get(i).getSex());
                                }
                            }
                        }
                    }
                });
                break;
            case R.id.btn_update:
                MyData myData2 = new MyData();
                myData2.setName("李四");
                myData2.update("704147f0c9", new UpdateListener() {
                    @Override
                    public void done(BmobException e) {
                        if(e==null){
                            Toast.makeText(TestActivity.this,"更新成功",Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(TestActivity.this,"更新失败",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
        }
    }
}
