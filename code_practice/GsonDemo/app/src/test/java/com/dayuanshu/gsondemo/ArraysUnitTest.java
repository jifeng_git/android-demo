package com.dayuanshu.gsondemo;

import com.dayuanshu.gsondemo.bean.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.junit.Test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ArraysUnitTest {

    @Test
    public void testArrays(){
        User[] users = new User[3];
        // Java对象
        users[0]=new User("肖战","123123",19,false);
        users[1]=new User("王一博","123123",20,true);
        //Gson提供的gson对象
        Gson gson = new Gson();
        // 序列化
        String toJson = gson.toJson(users);
        System.out.println("序列化:"+toJson);

        // 反序列化
        User[] users1 = gson.fromJson(toJson, User[].class);
        System.out.println("反序列化0:"+users1[0]);
        System.out.println("反序列化1:"+users1[1]);
        System.out.println("反序列化2:"+users1[2]);

    }

    @Test
    public void testListObject(){
        List<User> userList=new ArrayList<>();
        userList.add(new User("赵丽颖","123",22,false));
        userList.add(new User("陈伟霆","123",22,false));
        userList.add(null);
        //Gson提供的gson对象
        Gson gson = new Gson();
        // 序列化
        String toJson = gson.toJson(userList);
        System.out.println("序列化:"+toJson);
        // 反序列化
        Type type=new TypeToken<List<User>>(){}.getType();
        List<User> list2 = gson.fromJson(toJson, type);
        System.out.println("反序列化0:"+list2.get(0));
        System.out.println("反序列化1:"+list2.get(1));
        System.out.println("反序列化2:"+list2.get(2));
    }
}
