package com.dayuanshu.gsondemo;

import com.dayuanshu.gsondemo.bean.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Test;

public class AnnotationUnitTest {

    @Test
    public void testExpose(){
        User zs = new User("张三", "123", 18, false);
        zs.setTest1(1);
        zs.setTest2(2);

        // Gson提供的gson对象
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        // 序列化
        String toJson = gson.toJson(zs);
        System.out.println("序列化:"+toJson);

        //反序列化
        User user = gson.fromJson(toJson, User.class);
        System.out.println("反序列化:"+user);
    }
}
