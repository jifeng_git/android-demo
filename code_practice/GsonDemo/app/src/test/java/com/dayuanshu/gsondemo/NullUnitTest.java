package com.dayuanshu.gsondemo;

import com.dayuanshu.gsondemo.bean.User;
import com.google.gson.Gson;

import org.junit.Test;

public class NullUnitTest {

    @Test
    public void testNull(){
        User zs = new User("张三", "123", 18, false);

        //Gson的gson序列化,当某一个属性为null时,gson会忽略这个属性,集合中有null,gson不会忽略
        Gson gson = new Gson();
        String toJson = gson.toJson(zs);
        System.out.println("序列化:"+toJson);
        // 反序列化
        User user = gson.fromJson(toJson, User.class);
        System.out.println("反序列化:"+user);
    }
}
