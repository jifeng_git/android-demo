package com.dayuanshu.gsondemo;

import com.dayuanshu.gsondemo.bean.Job;
import com.dayuanshu.gsondemo.bean.User;
import com.google.gson.Gson;

import org.junit.Test;

public class ObjectUnitTest {

    @Test
    public void testObject(){
        // java对象
        User zs = new User("张三", "123", 18, false);
        //Google提供的Gson对象
        Gson gson = new Gson();
        // 序列化
        String json = gson.toJson(zs);
        System.out.println("序列化后的结果:"+json);
        // 反序列化
        User user = gson.fromJson(json, User.class);
        System.out.println("反序列化为一个对象:"+user.getUserName()+",密码:"+user.getPassword());
    }

    @Test
    public void testNestedObject(){
        // java对象
        User ls = new User("李四", "123", 18, false);
        Job gongren = new Job("工人", 10000);
        ls.setJob(gongren);
        // Gson提供的gson序列化
        Gson gson = new Gson();
        // 序列化
        String lisi = gson.toJson(ls);
        System.out.println("序列化:"+lisi);

        // 反序列化
        User user = gson.fromJson(lisi, User.class);
        System.out.println("反序列化的对象:"+user.getUserName()+",年龄:"+user.getAge()+",职位类型:"+user.getJob());
    }
}
