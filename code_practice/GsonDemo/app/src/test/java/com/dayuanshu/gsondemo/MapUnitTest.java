package com.dayuanshu.gsondemo;

import com.dayuanshu.gsondemo.bean.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.junit.Test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapUnitTest {

    @Test
    public void testMapObject(){
        Map<String,User> userMap=new HashMap();
        userMap.put("1",new User("赵丽颖","123",22,false));
        userMap.put("2",new User("陈伟霆","123",22,false));
        userMap.put(null,null);
        //Gson提供的gson对象
        Gson gson = new Gson();
        // 序列化
        String toJson = gson.toJson(userMap);
        System.out.println("序列化:"+toJson);
        // 反序列化
        Type type=new TypeToken<Map<String,User>>(){}.getType();
        Map<String,User> map2 = gson.fromJson(toJson, type);
        System.out.println("反序列化0:"+map2.get("1"));
        System.out.println("反序列化1:"+map2.get("2"));
        System.out.println("反序列化2:"+map2.get(2));
    }

    @Test
    public void testSetObject(){
        Set<User> userSet=new HashSet<>();
        userSet.add(new User("赵丽颖","123",22,false));
        userSet.add(new User("陈伟霆","123",22,false));
        userSet.add(null);

        //Gson提供的gson对象
        Gson gson = new Gson();

        // 序列化
        String toJson = gson.toJson(userSet);
        System.out.println("序列化:"+toJson);

        // 反序列化
        Type type=new TypeToken<Set<User>>(){}.getType();
        Set<User> set2 = gson.fromJson(toJson, type);
        Iterator<User> iterator = set2.iterator();
        while (iterator.hasNext()){
            User next = iterator.next();
            System.out.println("反序列化:"+next);
        }

    }
}
