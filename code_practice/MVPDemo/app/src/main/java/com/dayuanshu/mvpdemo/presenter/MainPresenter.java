package com.dayuanshu.mvpdemo.presenter;

import com.dayuanshu.mvpdemo.IView;
import com.dayuanshu.mvpdemo.model.Callback;
import com.dayuanshu.mvpdemo.model.IModel;
import com.dayuanshu.mvpdemo.model.MainModel;
import com.dayuanshu.mvpdemo.model.User;

/**
 * P层实现类
 */
public class MainPresenter implements IPresenter, Callback {
    private IView mainView;
    private IModel mainModel;

    public MainPresenter(IView mainView){
        mainModel=new MainModel();
        this.mainView=mainView;
    }
    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     */
    @Override
    public void login(String username, String password) {
        mainView.showProgress();
        // 把V层发过来的数据交给M层去获取数据
        mainModel.login(username,password,this);
    }

    /**
     * 登录成功的回调,把处理的结果通知给P层
     * @param user
     */
    @Override
    public void onSuccess(User user){
        mainView.hideProgress();
        // 通知成功
        mainView.loginSuccess(user);
    }

    /**
     * 登录失败的回调
     * @param msg
     */
    @Override
    public void onFailure(String msg){
        mainView.hideProgress();
        // 通知失败
        mainView.loginFailure(msg);
    }
}
