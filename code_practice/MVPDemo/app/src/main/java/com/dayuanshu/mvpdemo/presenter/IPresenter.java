package com.dayuanshu.mvpdemo.presenter;

import com.dayuanshu.mvpdemo.model.User;

/**
 * P层能够处理登录
 */
public interface IPresenter {

    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     */
    void login(String username,String password);
    void onSuccess(User user);
    void onFailure(String msg);
}
