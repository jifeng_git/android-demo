package com.dayuanshu.mvpdemo;

import com.dayuanshu.mvpdemo.model.User;

/**
 * V层
 */
public interface IView {
    /**
     * 显示进度条
     */
    void showProgress();

    /**
     * 隐藏进度条
     */
    void hideProgress();

    /**
     * 登录成功后响应的结果
     * @param user
     */
    void loginSuccess(User user);

    /**
     * 登录失败显示的结果
     * @param msg
     */
    void loginFailure(String msg);
}
