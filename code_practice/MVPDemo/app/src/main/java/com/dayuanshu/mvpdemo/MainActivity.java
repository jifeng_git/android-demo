package com.dayuanshu.mvpdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dayuanshu.mvpdemo.model.MainModel;
import com.dayuanshu.mvpdemo.model.User;
import com.dayuanshu.mvpdemo.presenter.IPresenter;
import com.dayuanshu.mvpdemo.presenter.MainPresenter;

import javax.security.auth.callback.Callback;

/**
 * V层
 * 绝大多数工作都是Activity或者Fragment中完成的,使得Activity或者Fragment变的非常重
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener,IView {
    /**
     * 用户名输入框
     */
    private EditText et_username;
    /**
     * 密码
     */
    private EditText et_password;
    /**
     * 登录按钮
     */
    private Button btn_login;
    /**
     * 进度条
     */
    private ProgressDialog progressDialog;
    /**
     * Model层数据
     */
//    private MainModel mainModel;
    /**
     * 让V层持有P层接口的数据,完成activity和fragment的解耦
     */
    private IPresenter mainPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 设置布局
        setContentView(R.layout.activity_main);
        /**
         * 获取用户名
         */
        et_username=findViewById(R.id.et_username);
        /**
         * 获取输入框的密码
         */
        et_password=findViewById(R.id.et_password);
        /**
         * 获取登录按钮
         */
        btn_login=findViewById(R.id.btn_login);


        // 更新UI
        /**
         * 按钮点击事件
         */
        btn_login.setOnClickListener(this);
        /**
         * 进度条
         */
        progressDialog=new ProgressDialog(this);
        // 设置进度条的样式
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // 设置对话框是否可以取消
        progressDialog.setCancelable(false);
        // 设置进度条描述信息
        progressDialog.setMessage("请稍后......");


        //mainModel=new MainModel();
        mainPresenter=new MainPresenter(this);
    }

    /**
     * 登录按钮点击事件,直接完成了业务逻辑的处理,有C层功能
     * @param v
     */
    @Override
    public void onClick(View v) {
        // 获取用户名
        String username = et_username.getText().toString();
        // 获取密码
        String password = et_password.getText().toString();
        // 显示进度条
        progressDialog.show();
        // 把事件发给P层
        mainPresenter.login(username,password);
        /*mainModel.login(username, password, new Callback() {
           @Override
           public void onSuccess(User user){
               // 关闭对话框,从屏幕上移除
               progressDialog.dismiss();
               Toast.makeText(MainActivity.this,"登录成功",Toast.LENGTH_SHORT).show();
           }
           @Override
           public void onFailure(String msg){
               progressDialog.dismiss();
               Toast.makeText(MainActivity.this,"登录失败",Toast.LENGTH_SHORT).show();
           }
       });*/
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void loginSuccess(User user) {
        Toast.makeText(this,"成功",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginFailure(String msg) {
        Toast.makeText(this,"失败",Toast.LENGTH_SHORT).show();
    }
}