package com.dayuanshu.mvpdemo.model;


/**
 * M层与P层也完成解耦
 */
public interface IModel {

    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     * @param callback 回调函数
     */
    void login(String username, String password, Callback callback);
}
