package com.dayuanshu.mvpdemo.model;

public interface Callback {
    void onSuccess(User user);
}
