package com.dayuanshu.mvpdemo.model;

import android.os.Handler;
import android.os.Looper;

/**
 * Model层:数据处理
 */
public class MainModel implements IModel {

    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     * @param callback 回调
     */
    @Override
    public void login(final String username, String password, final Callback callback){
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable(){
            @Override
            public void run() {
                callback.onSuccess(new User(1,"jifeng"));
            }
        },2000);
    }

}
