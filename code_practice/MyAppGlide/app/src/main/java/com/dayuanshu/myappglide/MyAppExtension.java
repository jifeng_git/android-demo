package com.dayuanshu.myappglide;

import com.bumptech.glide.annotation.GlideExtension;
import com.bumptech.glide.annotation.GlideOption;
import com.bumptech.glide.load.resource.bitmap.Rotate;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;

@GlideExtension
public class MyAppExtension {
    private MyAppExtension(){

    }
    @GlideOption
    public static BaseRequestOptions<?> defaultImg(BaseRequestOptions<?> options){
        DrawableCrossFadeFactory factory = new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();

        return options.placeholder(R.drawable.find2).error(R.drawable.msg2)
                .fallback(R.drawable.mine2).transform(new Rotate(90));
    }
}
