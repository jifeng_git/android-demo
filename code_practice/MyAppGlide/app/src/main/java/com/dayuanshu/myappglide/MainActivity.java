package com.dayuanshu.myappglide;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.bumptech.glide.load.resource.bitmap.Rotate;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imageView = (ImageView) findViewById(R.id.my_image_view);

        RequestOptions sharedOptions =
                new RequestOptions()
                        .placeholder(R.drawable.find2)
                        .error(R.drawable.msg2)
                        .fallback(R.drawable.mine2)
                        .override(100,100);

        /*Glide.with(this)
                .load("https://pics1.baidu.com/feed/9d82d158ccbf6c818a26c91c99a41a3f32fa4051.jpeg?token=75682766f6f0b9ea97e800d9fe97bbd5")
                .apply(sharedOptions)
                .into(imageView);*/

        /*Glide.with(this)
                .load("https://pics1.baidu.com/feed/9d82d158ccbf6c818a26c91c99a41a3f32fa4051.jpeg?token=75682766f6f0b9ea97e800d9fe97bbd5")
                .apply(sharedOptions)
                .transition(DrawableTransitionOptions.withCrossFade(4000))
                .into(imageView);*/
        // 交叉淡入
       /* DrawableCrossFadeFactory factory = new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();
        Glide.with(this)
                .load("https://pics1.baidu.com/feed/9d82d158ccbf6c818a26c91c99a41a3f32fa4051.jpeg?token=75682766f6f0b9ea97e800d9fe97bbd5")
                .apply(sharedOptions)
                .transition(DrawableTransitionOptions.withCrossFade(factory))
                .into(imageView);*/

        // 圆形效果
       /* DrawableCrossFadeFactory factory = new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();
        Glide.with(this)
                .load("https://pics1.baidu.com/feed/9d82d158ccbf6c818a26c91c99a41a3f32fa4051.jpeg?token=75682766f6f0b9ea97e800d9fe97bbd5")
                .apply(sharedOptions)
                .transition(DrawableTransitionOptions.withCrossFade(factory))
                .transform(new CircleCrop())
                .into(imageView);*/
        // 圆角效果
       /* DrawableCrossFadeFactory factory = new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();
        Glide.with(this)
                .load("https://pics1.baidu.com/feed/9d82d158ccbf6c818a26c91c99a41a3f32fa4051.jpeg?token=75682766f6f0b9ea97e800d9fe97bbd5")
                .apply(sharedOptions)
                .transition(DrawableTransitionOptions.withCrossFade(factory))
                .transform(new RoundedCorners(30))
                .into(imageView);*/
        // 左边圆形右边带点幅度的圆角
        /*DrawableCrossFadeFactory factory = new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();
        Glide.with(this)
                .load("https://pics1.baidu.com/feed/9d82d158ccbf6c818a26c91c99a41a3f32fa4051.jpeg?token=75682766f6f0b9ea97e800d9fe97bbd5")
                .apply(sharedOptions)
                .transition(DrawableTransitionOptions.withCrossFade(factory))
                .transform(new GranularRoundedCorners(30,80,80,30))
                .into(imageView);*/
        // 旋转效果
        DrawableCrossFadeFactory factory = new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();
        Glide.with(this)
                .load("https://pics1.baidu.com/feed/9d82d158ccbf6c818a26c91c99a41a3f32fa4051.jpeg?token=75682766f6f0b9ea97e800d9fe97bbd5")
                .apply(sharedOptions)
                .transition(DrawableTransitionOptions.withCrossFade(factory))
                .transform(new Rotate(90))
                .into(imageView);

//        GlideApp.with(this).load("").placeholder();
        /*GlideApp.with(this).load("")
                .defaultImg().into(imageView);*/
    }
}