package com.dayuanshu.myspdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    SharedPreferences sp;
    private EditText userName;
    private EditText password;
    private CheckBox cb_remeberpwd;
    private CheckBox cb_autologin;
    private Button btn_login;
    private Button btn_register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        // 获取首选项SP
        sp = getSharedPreferences("config", Context.MODE_PRIVATE);
        // 初始化
        initView();
        // TODO:回显数据
        // 第二次or第N次 打开的时候,从SP获取之前保存的数据,进行画面的同步,如果是null,就会返回默认值
        boolean remeberpwd = sp.getBoolean("remeberpwd", false);
        boolean autologin = sp.getBoolean("autologin", false);
        // 记住密码 之前勾上了
        if(remeberpwd){
            // 获取sp里面的 username 和password 并保存到EdText
            String username = sp.getString("username", "");
            String pwd2 = sp.getString("password", "");
            userName.setText(username);
            password.setText(pwd2);
            // 记住密码打钩
            cb_remeberpwd.setChecked(true);
        }
        // 自动登录,之前勾上了
        if(autologin){
            cb_autologin.setChecked(true);
            // 模拟自动登录
            Toast.makeText(this,"我自动登录了",Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 初始化
     */
    private void initView() {
        // 1.找到控件
        userName=findViewById(R.id.user_name);
        password=findViewById(R.id.pwd);
        cb_remeberpwd=findViewById(R.id.cb_remeberpwd);
        cb_autologin=findViewById(R.id.cb_autologin);
        btn_register=findViewById(R.id.btn_register);
        btn_login=findViewById(R.id.btn_login);
        // 2.设置监听
        MyOnclickListener onclickListener=new MyOnclickListener();
        btn_login.setOnClickListener(onclickListener);
        btn_register.setOnClickListener(onclickListener);
    }
    private class MyOnclickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btn_register:
                    break;
                // 登录按钮 登录流程
                case R.id.btn_login:
                    // 登录操作
                    String username = userName.getText().toString().trim();
                    String pwd = password.getText().toString().trim();
                    if(TextUtils.isEmpty(username)||TextUtils.isEmpty(pwd)){
                        Toast.makeText(MainActivity2.this,"用户名或者密码为空...",Toast.LENGTH_SHORT).show();
                    }else {
                        // 1.记住密码是否勾选
                        if(cb_remeberpwd.isChecked()){
                            // 用户名跟密码都需要保存到SP中,同时记住密码的状态也需要保存
                            SharedPreferences.Editor editor=sp.edit();
                            editor.putString("username",username);
                            editor.putString("password",pwd);
                            // 记住密码的状态
                            editor.putBoolean("remeberpwd",true);
                            editor.apply();
                        }
                        // 2.自动登录是否勾选
                        if(cb_autologin.isChecked()){
                            SharedPreferences.Editor editor=sp.edit();
                            // 记住自动登录的状态
                            editor.putBoolean("autologin",true);
                            editor.apply();
                        }
                    }
                    break;
            }
        }
    }
}