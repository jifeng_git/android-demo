package com.dayuanshu.myspdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * 数据保存到SP
     * 参数1:SP的名字
     * 参数2:SP保存的时候用的模式(追加(每次保存都会追加到后面Jifeng,第二次:JifengAAA==Context.MODE_PRIVATE),
     *      常规(每次保存都会更新BBB=Context.MODE_APPEND))
     * public SharedPreferences getSharedPreferences(String name, int mode) {
     *         return mBase.getSharedPreferences(name, mode);
     *     }
     * @param view
     */
    public void savaToSP(View view) {
        SharedPreferences sp = getSharedPreferences("JiFengName", Context.MODE_PRIVATE);
        sp.edit().putString("JiFengKey","吸星大法").apply();//apply()才会写入到配置文件中去
    }

    /**
     * 从SP获取数据
     * @param view
     */
    public void getSPData(View view) {
        SharedPreferences sp = getSharedPreferences("JiFengName", Context.MODE_PRIVATE);
        String value=sp.getString("JiFengKey","默认值");// 假设获取到JiFengKey是空的,就会使用默认值返回
        Toast.makeText(this,"获取SP的值是:"+value,Toast.LENGTH_SHORT).show();
    }
}