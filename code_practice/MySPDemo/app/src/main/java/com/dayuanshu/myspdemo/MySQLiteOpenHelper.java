package com.dayuanshu.myspdemo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * 数据库连接工具类  单例模式(1.构造函数私有化 2.对外提供方法)
 */
public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    // 1.构造函数私有化
    public MySQLiteOpenHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    // 2.对外提供方法访问 单例模式
    private static SQLiteOpenHelper mInstance;
    public static synchronized SQLiteOpenHelper getmInstance(Context context){
        if(mInstance==null){
            mInstance=new MySQLiteOpenHelper(context,"jifengDB.db",null,1);// 以后想要数据库升级,修改成2,修改成3
        }
        return mInstance;
    }

    /**
     * 创建表 表数据初始化
     * 数据库初始化使用,数据库第一次创建的时候调用,第二次发现有了,就不会创建了,意味着子函数只会执行一次
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // 创建表:person表 _id name
        // 主键 _id标准的写法,只能使用Integer类型,必须唯一,自动增长
        String sql="create table persons(_id integer primary key autoincrement,name text)";
        db.execSQL(sql);
    }

    /**
     * 数据库升级使用
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
