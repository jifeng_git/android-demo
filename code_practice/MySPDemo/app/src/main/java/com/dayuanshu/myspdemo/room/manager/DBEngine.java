package com.dayuanshu.myspdemo.room.manager;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.dayuanshu.myspdemo.room.Student;
import com.dayuanshu.myspdemo.room.StudentDao;
import com.dayuanshu.myspdemo.room.StudentDatabase;

import java.util.List;

/**
 * DB的引擎
 */
public class DBEngine {

    // 只需要拿到dao,就可以对数据库进行增删改查
    private StudentDao studentDao;

    public DBEngine(Context context){
        StudentDatabase studentDatabase = StudentDatabase.getInstance(context);
       studentDao = studentDatabase.getStudentDao();
    }

    // dao增删改查

    /**
     * insert 插入student数据
     * @param students
     */
    public void insertStudents(Student ... students){
        new InsertAsyncTask(studentDao).execute(students);
    }

    /**
     * update 更新student数据
     * @param students
     */
    public void updateStudents(Student ... students){
        new UpdateAsyncTask(studentDao).execute(students);
    }

    /**
     * delete 根据条件删除student数据
     * @param students
     */
    public void deleteStudents(Student ... students){
        new DeleteAsyncTask(studentDao).execute(students);
    }

    /**
     *  delete 删除所有的student数据
     */
    public void deleteAllStudents(){
        new DeleteAllAsyncTask(studentDao).execute();
    }

    /**
     * queryAll 查询所有的student
     */
    public void queryAllStudents(){
        new QueryAllAsyncTask(studentDao).execute();
    }


    // 如果我们想玩数据库,默认是异步线程,===================异步操作

    /**
     * insert 插入
     */
    private static class InsertAsyncTask extends AsyncTask<Student,Void,Void>{

        private StudentDao dao;

        public InsertAsyncTask(StudentDao studentDao) {
            dao=studentDao;
        }

        @Override
        protected Void doInBackground(Student... students) {
            dao.insertStudents(students);
            return null;
        }
    }

    /**
     * update 更新
     */
    private static class UpdateAsyncTask extends AsyncTask<Student,Void,Void>{
        private StudentDao dao;

        public UpdateAsyncTask(StudentDao studentDao) {
            dao=studentDao;
        }

        @Override
        protected Void doInBackground(Student... students) {
            dao.updateStudents(students);
            return null;
        }
    }

    /**
     * delete 条件删除
     */
    private static class DeleteAsyncTask extends AsyncTask<Student,Void,Void>{
        private StudentDao dao;

        public DeleteAsyncTask(StudentDao studentDao) {
            dao=studentDao;
        }

        /**
         * 传递了Student,就是条件删除
         * @param students
         * @return
         */
        @Override
        protected Void doInBackground(Student... students) {
            dao.deleteAllStudents(students);
            return null;
        }
    }

    /**
     * delete 全部删除
     */
    private static class DeleteAllAsyncTask extends AsyncTask<Void,Void,Void>{
        private StudentDao dao;

        public DeleteAllAsyncTask(StudentDao studentDao) {
            dao=studentDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.deleteAllStudents();
            return null;
        }
    }

    /**
     * query 全部查询
     */
    private static class QueryAllAsyncTask extends AsyncTask<Void,Void,Void>{
        private StudentDao dao;

        public QueryAllAsyncTask(StudentDao studentDao) {
            dao=studentDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            List<Student> allStudent = dao.getAllStudent();
            for (Student student : allStudent) {
                Log.e("dys","doInBackground: 全部查询每一项:"+student);
            }
            return null;
        }
    }
}
