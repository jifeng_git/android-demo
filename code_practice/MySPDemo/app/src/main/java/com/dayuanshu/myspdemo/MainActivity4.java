package com.dayuanshu.myspdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.dayuanshu.myspdemo.room.Student;
import com.dayuanshu.myspdemo.room.manager.DBEngine;

public class MainActivity4 extends AppCompatActivity {
    private DBEngine dbEngine;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        // 初始化
        dbEngine=new DBEngine(this);
    }

    /**
     * 插入
     * @param view
     */
    public void insertAction(View view) {
        Student student1 = new Student("赵丽颖",22);
        Student student2 = new Student("李易峰",25);
        Student student3 = new Student("陈伟霆",28);
        dbEngine.insertStudents(student1,student2,student3);
    }

    /**
     * 修改: 修改下标为3的数据
     * @param view
     */
    public void updateAction(View view) {
        Student student4 = new Student("王一博",23);
        student4.setId(3);
        dbEngine.updateStudents(student4);
    }

    /**
     * 条件删除 删除下标为3的数据
     * @param view
     */
    public void deleteAction(View view) {
        Student student = new Student(null,0);
        student.setId(3);
        dbEngine.deleteStudents(student);
    }

    /**
     * 查询
     * @param view
     */
    public void queryAction(View view) {
        dbEngine.queryAllStudents();
    }

    /**
     * 全部删除
     * @param view
     */
    public void deleteAllAction(View view) {
        dbEngine.deleteAllStudents();
    }
}