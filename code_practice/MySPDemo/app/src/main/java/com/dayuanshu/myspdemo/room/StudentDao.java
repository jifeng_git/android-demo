package com.dayuanshu.myspdemo.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao // 对表进行增删改查
public interface StudentDao {
    // 增
    @Insert
    void insertStudents(Student ... students);

    // 条件删除
    @Delete
    void deleteAllStudents(Student ... students);
    // 删@Delete 单个条件删除使用
    @Query("delete from Student")
    void deleteAllStudents();

    // 改
    @Update
    void updateStudents(Student ... students);

    // 查询所有
    @Query("select * from Student order by id desc")
    List<Student> getAllStudent();

}
