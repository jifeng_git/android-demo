package com.dayuanshu.myspdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }

    /**
     * 生成DB文件
     * @param view
     */
    public void createDB(View view) {
        SQLiteOpenHelper helper = MySQLiteOpenHelper.getmInstance(this);
        // database文件夹的创建靠下面这句话(helper.getReadableDatabase()||helper.getWritableDatabase())
        SQLiteDatabase readableDatabase = helper.getReadableDatabase();
    }

    /**
     * 查询db文件
     * @param view
     */
    public void queryDB(View view) {
        SQLiteOpenHelper helper = MySQLiteOpenHelper.getmInstance(this);
        // database文件夹的创建靠下面这句话(helper.getReadableDatabase()||helper.getWritableDatabase())
        SQLiteDatabase db = helper.getWritableDatabase();
        // 数据库打开成功 返回true 进入if
        if(db.isOpen()){
            // 返回一个游标
            Cursor cursor = db.rawQuery("select * from persons", null);
            // 迭代游标,往下面移动,遍历数据
            while (cursor.moveToNext()){
//                int _id = rawQuery.getInt(0);
//                int _id = rawQuery.getString(1);

                @SuppressLint("Range") int _id=cursor.getInt((cursor.getColumnIndex("_id")));
                @SuppressLint("Range") String name=cursor.getString(cursor.getColumnIndex("name"));
                Log.d("dys","query:_id:==>"+_id+",name:==>"+name);
                // 关闭游标,否则耗性能
                cursor.close();
                db.close();
            }

        }
    }

    /**
     * 插入数据到数据库
     * @param view
     */
    public void insertRow(View view) {
        SQLiteOpenHelper helper = MySQLiteOpenHelper.getmInstance(this);
        // database文件夹的创建靠下面这句话(helper.getReadableDatabase()||helper.getWritableDatabase())
        SQLiteDatabase db = helper.getWritableDatabase();
        // 确保数据库是打开的
        if(db.isOpen()){
            String sql="insert into persons(name) values('大源数')";
            db.execSQL(sql);
            // 关闭数据库连接
            db.close();
        }

    }

    /**
     * 修改数据,修改第三条数据为阿里巴巴
     * @param view
     */
    public void updateRow(View view) {
        SQLiteOpenHelper helper = MySQLiteOpenHelper.getmInstance(this);
        // database文件夹的创建靠下面这句话(helper.getReadableDatabase()||helper.getWritableDatabase())
        SQLiteDatabase db = helper.getWritableDatabase();
        // 确保数据库是打开的
        if(db.isOpen()){
            // 修改语句
            String sql="update persons set name=? where _id=?";
            db.execSQL(sql,new Object[]{"阿里巴巴",3});
            // 关闭数据库连接
            db.close();
        }

    }

    /**
     * 删除数据 删除第三条数据
     * @param view
     */
    public void deleteRow(View view) {
        SQLiteOpenHelper helper = MySQLiteOpenHelper.getmInstance(this);
        // database文件夹的创建靠下面这句话(helper.getReadableDatabase()||helper.getWritableDatabase())
        SQLiteDatabase db = helper.getWritableDatabase();
        // 确保数据库是打开的
        if(db.isOpen()){
            // 删除语句
            String sql="delete from persons where _id=?";
            db.execSQL(sql,new Object[]{3});
            // 关闭数据库连接
            db.close();
        }

    }
}