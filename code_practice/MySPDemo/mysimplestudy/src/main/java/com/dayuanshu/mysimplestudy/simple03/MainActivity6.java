package com.dayuanshu.mysimplestudy.simple03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.dayuanshu.mysimplestudy.R;

public class MainActivity6 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);
        Intent intent = getIntent();
        Student student=(Student) intent.getSerializableExtra("Student");
        // 提示显示
        Toast.makeText(this,"Student对象的值是:===>"+student,Toast.LENGTH_SHORT).show();
    }
}