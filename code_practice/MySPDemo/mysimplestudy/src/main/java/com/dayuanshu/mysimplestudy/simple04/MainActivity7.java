package com.dayuanshu.mysimplestudy.simple04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.dayuanshu.mysimplestudy.R;

public class MainActivity7 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);
    }

    /**
     * 跳转到simple04.MainActivity8
     * @param view
     */
    public void startAction4(View view) {
        Intent intent = new Intent(this,MainActivity8.class);
        User user = new User();
        user.setUsername("王一博");
        user.setAge(22);
        user.setPassword("123434");
        // 传递对象---让intent携带过去
        intent.putExtra("user",user);
        startActivity(intent);
    }
}