package com.dayuanshu.mysimplestudy.simple03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.dayuanshu.mysimplestudy.R;

public class MainActivity5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);
    }

    /**
     * 跳转到simple03.MainActivity6
     * @param view
     */
    public void startAction3(View view) {
        Intent intent = new Intent(this, MainActivity6.class);
        Student student = new Student();
        student.setId(1001);
        student.setName("大源数");
        student.setAge(23);
        intent.putExtra("Student",student);
        //传递对象到MainActivity6
        startActivity(intent);
    }
}