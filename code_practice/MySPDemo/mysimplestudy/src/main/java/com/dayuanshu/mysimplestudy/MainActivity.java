package com.dayuanshu.mysimplestudy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * 跳转
     * @param view
     */
    public void startAction(View view) {
        Intent intent = new Intent(this,MainActivity2.class);
        // 跳转到MainActivity2时,携带数据
        intent.putExtra("name","jifeng");
        intent.putExtra("sex",'M');
        startActivity(intent);
    }
}