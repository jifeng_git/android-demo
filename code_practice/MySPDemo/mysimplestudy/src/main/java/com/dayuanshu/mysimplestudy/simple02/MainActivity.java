package com.dayuanshu.mysimplestudy.simple02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.dayuanshu.mysimplestudy.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }

    /**
     * 跳转2:跳转到simple02.MainActivity2
     * @param view
     */
    public void startAction2(View view) {
        Intent intent = new Intent(this, MainActivity3.class);
        // 封装一个Bundle包裹对象
        Bundle bundle = new Bundle();
        bundle.putString("name","疾风2");
        bundle.putChar("sex",'男');
        // put xxx100个字段
        // intent携带我们的bundle对象
        intent.putExtras(bundle);
        startActivity(intent);
    }
}