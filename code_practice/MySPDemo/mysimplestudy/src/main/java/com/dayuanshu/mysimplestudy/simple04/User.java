package com.dayuanshu.mysimplestudy.simple04;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 此对象称为Parcelable的子类,就具备传递的资格
 */
public class User implements Parcelable {
    private String username;
    private String password;
    private int age;
    public User(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 从Parcel对象里面读取出来,赋值给成员
     * @param in
     */
    protected User(Parcel in) {
        // 从Parcel对象里面读取成员,赋值给username,password,age
        username = in.readString();
        password = in.readString();
        age = in.readInt();
    }

    /**
     * 把属性写入到Parcel对象中去
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(password);
        dest.writeInt(age);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        /**
         * 创建User对象,并且把Parcel构建好,传递给我们的User(成员数据就可以从Parcel中获取了)
         * @param in
         * @return
         */
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                '}';
    }
}
