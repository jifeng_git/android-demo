package com.dayuanshu.mysimplestudy.simple04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.dayuanshu.mysimplestudy.R;

public class MainActivity8 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main8);
        Intent intent = getIntent();
        User user=intent.getParcelableExtra("user");
        // 显示MainActivity7传递过来的对象里面的数据
        Toast.makeText(this,"user对象的值是:"+user,Toast.LENGTH_SHORT).show();
    }
}