package com.dayuanshu.mysimplestudy.simple02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.dayuanshu.mysimplestudy.R;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        Intent intent = getIntent();
        // 自动拆开Bundle对象了,获取里面的name,sex
        String name = intent.getStringExtra("name");
        char sex = intent.getCharExtra("sex", '女');
        Toast.makeText(this,"name:==>"+name+"|sex:==>"+sex,Toast.LENGTH_SHORT).show();

    }
}