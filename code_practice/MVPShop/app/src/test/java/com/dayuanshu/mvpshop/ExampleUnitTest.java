package com.dayuanshu.mvpshop;

import org.junit.Test;

import static org.junit.Assert.*;

import com.dayuanshu.mvpshop.bean.BaseBean;
import com.dayuanshu.mvpshop.bean.Goods;
import com.dayuanshu.mvpshop.network.RetrofitClient;
import com.dayuanshu.mvpshop.network.service.GoodsService;

import java.util.List;

import io.reactivex.rxjava3.functions.Consumer;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
        GoodsService goodsService = RetrofitClient.getInstance().getService(GoodsService.class);
        goodsService.getGoods().subscribe(new Consumer<BaseBean<List<Goods>>>() {
            @Override
            public void accept(BaseBean<List<Goods>> listBaseBean) throws Throwable {
                System.out.println("测试成功");
                System.out.println(listBaseBean);
            }
        });
        while (true){}
    }
}