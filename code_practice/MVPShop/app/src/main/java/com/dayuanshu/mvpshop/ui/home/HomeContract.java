package com.dayuanshu.mvpshop.ui.home;

import com.dayuanshu.mvpshop.bean.BaseBean;
import com.dayuanshu.mvpshop.bean.Goods;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;

/**
 * 契约类
 */
public interface HomeContract {

    interface IHomePresenter{
        void getData();
    }
    interface IHomeModel{
        Flowable<BaseBean<List<Goods>>> getData();
    }
    interface IHomeView{
        void getGoodsSuccess(List<Goods> goods);
        void getGoodsError(Throwable throwable);
    }
}
