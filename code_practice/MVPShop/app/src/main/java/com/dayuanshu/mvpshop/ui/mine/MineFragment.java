package com.dayuanshu.mvpshop.ui.mine;

import com.dayuanshu.mvpshop.R;
import com.dayuanshu.mvpshop.ui.base.BaseFragment;

public class MineFragment extends BaseFragment {
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initViews() {

    }
}
