package com.dayuanshu.mvpshop.ui.home;

import com.dayuanshu.mvpshop.bean.BaseBean;
import com.dayuanshu.mvpshop.bean.Goods;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class HomePresenter implements HomeContract.IHomePresenter {

    private HomeContract.IHomeView homeView;
    private HomeContract.IHomeModel homeModel;

    public HomePresenter(HomeContract.IHomeView homeView){
        this.homeView=homeView;
        homeModel = new HomeModel();
    }
    @Override
    public void getData() {
        homeModel.getData()
                // 数据请求在IO
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<BaseBean<List<Goods>>>() {
                    /**
                     * 获取数据成功
                     * @param listBaseBean
                     * @throws Throwable
                     */
                    @Override
                    public void accept(BaseBean<List<Goods>> listBaseBean) throws Throwable {
                        homeView.getGoodsSuccess(listBaseBean.getData());
                    }
                }, new Consumer<Throwable>() {
                    /**
                     * 获取数据失败
                     * @param throwable
                     * @throws Throwable
                     */
                    @Override
                    public void accept(Throwable throwable) throws Throwable {
                        homeView.getGoodsError(throwable);
                    }
                });
    }
}
