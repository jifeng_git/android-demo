package com.dayuanshu.mvpshop.ui.home;

import com.dayuanshu.mvpshop.bean.BaseBean;
import com.dayuanshu.mvpshop.bean.Goods;
import com.dayuanshu.mvpshop.network.RetrofitClient;
import com.dayuanshu.mvpshop.network.service.GoodsService;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;

public class HomeModel implements HomeContract.IHomeModel {
    // 从网络获取
    @Override
    public Flowable<BaseBean<List<Goods>>> getData() {
        return RetrofitClient.getInstance().getService(GoodsService.class).getGoods();
    }
}
