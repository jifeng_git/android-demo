package com.dayuanshu.mvpshop.ui.home;


import android.content.Intent;
import android.util.Log;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dayuanshu.mvpshop.R;
import com.dayuanshu.mvpshop.bean.Goods;
import com.dayuanshu.mvpshop.bean.GoodsDetail;
import com.dayuanshu.mvpshop.ui.base.BaseFragment;
import com.dayuanshu.mvpshop.ui.detail.GoodsDetailActivity;
import com.dayuanshu.mvpshop.ui.home.adapter.HomeRecyclerViewAdapter;
import com.dayuanshu.mvpshop.ui.home.adapter.HomeSpanSizeLookup;

import java.util.ArrayList;
import java.util.List;

/**
 * V:只负责View层的处理
 */
public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, HomeContract.IHomeView, HomeRecyclerViewAdapter.OnItemClickListener {
    private HomeRecyclerViewAdapter homeRecyclerViewAdapter;
    private HomePresenter homePresenter;
    private HomeSpanSizeLookup homeSpanSizeLookup;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initViews() {
        // 找到两个控件
        swipeRefreshLayout = find(R.id.home_swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(this);

        RecyclerView recyclerView = find(R.id.home_recycler);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        List<Goods> goods=new ArrayList<>();
        homeSpanSizeLookup = new HomeSpanSizeLookup(goods);
        gridLayoutManager.setSpanSizeLookup(homeSpanSizeLookup);

        recyclerView.setLayoutManager(gridLayoutManager);
        homeRecyclerViewAdapter = new HomeRecyclerViewAdapter(recyclerView,getActivity(), goods);
        // 设置监听
        homeRecyclerViewAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(homeRecyclerViewAdapter);

        homePresenter=new HomePresenter(this);
        // 初始化的时候要去获取一次数据
        homePresenter.getData();
    }

    /**
     * 每次刷新获取一次数据
     */
    @Override
    public void onRefresh() {
        // 刷新完成状态
        swipeRefreshLayout.setRefreshing(false);
        homePresenter.getData();
    }

    @Override
    public void getGoodsSuccess(List<Goods> goods) {
        homeSpanSizeLookup.setGoods(goods);
        homeRecyclerViewAdapter.setGoods(goods);
    }

    @Override
    public void getGoodsError(Throwable throwable) {

    }

    /**
     * 跳转到商品详情
     * @param goods
     */
    @Override
    public void onItemClick(Goods goods) {
        Log.i("dys","onItemClick:"+goods);
        Intent intent = new Intent(getActivity(), GoodsDetailActivity.class);
        intent.putExtra(GoodsDetailActivity.GOODS_ID,goods.getGoodsId());
        startActivity(intent);
    }
}
