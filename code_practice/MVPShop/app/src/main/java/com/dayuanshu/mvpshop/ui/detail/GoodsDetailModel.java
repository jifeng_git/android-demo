package com.dayuanshu.mvpshop.ui.detail;


import com.dayuanshu.mvpshop.bean.BaseBean;
import com.dayuanshu.mvpshop.bean.GoodsDetail;
import com.dayuanshu.mvpshop.network.RetrofitClient;
import com.dayuanshu.mvpshop.network.service.GoodsService;

import io.reactivex.rxjava3.core.Flowable;

public class GoodsDetailModel implements GoodsDetailContract.IGoodsDetailModel {


    @Override
    public Flowable<BaseBean<GoodsDetail>> getGoodsDetail(int goodsId) {
        return RetrofitClient.getInstance().getService(GoodsService.class)
                .getGoodsDetail(goodsId);
    }
}
