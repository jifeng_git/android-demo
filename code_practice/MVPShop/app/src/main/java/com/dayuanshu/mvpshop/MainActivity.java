package com.dayuanshu.mvpshop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.dayuanshu.mvpshop.ui.base.BaseActivity;
import com.dayuanshu.mvpshop.ui.cart.CartFragment;
import com.dayuanshu.mvpshop.ui.home.HomeFragment;
import com.dayuanshu.mvpshop.ui.mine.MineFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private Fragment[] fragments;
    private int lastFragmentIndex=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 一开始自动为我们生成
        setTheme(R.style.Theme_MVPShop);
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews() {
        BottomNavigationView bottomNavigationView=
                find(R.id.main_bottom_navigation);
        // 点击底部菜单,知道用户点击了哪一个,注册监听
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        fragments=new Fragment[]{
                new HomeFragment(),
                new CartFragment(),
                new MineFragment()};
        // 主页默认的Fragment
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.main_frame,fragments[0])
                .commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // 点中的item设置checked为true
        item.setChecked(true);
        // 点击不同的菜单,切换到不同的fragment
        switch (item.getItemId()){
            case R.id.bottom_home:
                switchFragment(0);
                break;
            case R.id.bottom_cart:
                switchFragment(1);
                break;
            case R.id.bottom_mine:
                switchFragment(2);
                break;
        }
        return false;
    }

    /**
     * 切换Fragment
     * @param to 切换到哪个Fragment
     */
    private void switchFragment(int to){
        if(lastFragmentIndex==to){return;}
        FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        if(!fragments[to].isAdded()){
            // 添加Fragment
            fragmentTransaction.add(R.id.main_frame,fragments[to]);
        }else {
            fragmentTransaction.show(fragments[to]);
        }
        // 把之前添加隐藏掉
        fragmentTransaction.hide(fragments[lastFragmentIndex]).commitAllowingStateLoss();
        lastFragmentIndex=to;
    }
}