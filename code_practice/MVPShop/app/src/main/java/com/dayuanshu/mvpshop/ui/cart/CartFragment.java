package com.dayuanshu.mvpshop.ui.cart;

import com.dayuanshu.mvpshop.R;
import com.dayuanshu.mvpshop.ui.base.BaseFragment;

public class CartFragment extends BaseFragment {
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_cart;
    }

    @Override
    protected void initViews() {

    }
}
