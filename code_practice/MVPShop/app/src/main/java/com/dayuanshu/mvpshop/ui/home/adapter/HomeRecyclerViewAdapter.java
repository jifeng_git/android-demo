package com.dayuanshu.mvpshop.ui.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.dayuanshu.mvpshop.R;
import com.dayuanshu.mvpshop.bean.Goods;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.indicator.CircleIndicator;

import java.util.List;

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private RecyclerView recyclerView;
    private Context context;
    private List<Goods> data;
    private LayoutInflater layoutInflater;
    private OnItemClickListener lister;

    public HomeRecyclerViewAdapter(RecyclerView recyclerView,Context context, List<Goods> data){
        this.recyclerView=recyclerView;
        this.context=context;
        this.data=data;
    }

    /**
     * 更新数据
     * @param data
     */
    public void setGoods(List<Goods> data){
        this.data.clear();
        // 布局加载器
        layoutInflater = LayoutInflater.from(context);
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(viewType, parent, false);
        // 设置监听
        view.setOnClickListener(this);
        if(viewType==R.layout.home_recycler_text_image){
           return new MultiViewHolder(view);
        }
        return new SingleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Goods goods = data.get(position);
        int itemViewType = getItemViewType(position);
        switch (itemViewType){
            case R.layout.home_recycler_banner:
                ((Banner) holder.itemView).setAdapter(new BannerImageAdapter<String>(goods.getBanners()) {
                            @Override
                            public void onBindView(BannerImageHolder holder, String data, int position, int size) {
                                //图片加载实现
                                Glide.with(holder.itemView)
                                        .load(data)
                                        .apply(RequestOptions.centerCropTransform())
                                        .into(holder.imageView);
                            }
                        })
                        .addBannerLifecycleObserver((LifecycleOwner) context)//添加生命周期观察者
                        .setIndicator(new CircleIndicator(context));// 小圆点
                break;
            case R.layout.home_recycler_image:
                Glide.with(holder.itemView)
                        .load(goods.getImageUrl())
                        .apply(RequestOptions.centerCropTransform())
                        .into((ImageView) holder.itemView);
                break;
            case R.layout.home_recycler_text:
                ((TextView)holder.itemView).setText(goods.getText());
                break;
            case R.layout.home_recycler_text_image:
                MultiViewHolder multiViewHolder=(MultiViewHolder) holder;
                multiViewHolder.textView.setText(goods.getText());
                Glide.with(holder.itemView)
                        .load(goods.getImageUrl())
                        .apply(RequestOptions.centerCropTransform())
                        .into(multiViewHolder.imageView);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        // 根据数据的类型
        Goods goods = data.get(position);
        if(goods.getBanners()!=null){
            // 显示广告位,无限循环的banner
            return R.layout.home_recycler_banner;
        }else if(goods.getImageUrl()==null){
            // 显示文字
            return R.layout.home_recycler_text;
        }else if(goods.getText()==null){
            // 显示图片
            return R.layout.home_recycler_image;
        }else {
            // 显示文字+图片
            return R.layout.home_recycler_text_image;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * 点击事件
     * @param v
     */
    @Override
    public void onClick(View v) {
        if(lister!=null){
            int position = recyclerView.getChildAdapterPosition(v);
            // 拿到对应下标的数据
            lister.onItemClick(data.get(position));
        }
    }

    class SingleViewHolder extends RecyclerView.ViewHolder{

        public SingleViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
    class MultiViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        ImageView imageView;
        public MultiViewHolder(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.text_view);
            imageView=itemView.findViewById(R.id.image_view);
        }
    }

    /**
     * 设置监听
     */
    public void setOnItemClickListener(OnItemClickListener listener){
        this.lister=listener;
    }
    /**
     * 点击商品的监听事件
     */
    public interface OnItemClickListener{
        void onItemClick(Goods goods);
    }
}
