package com.dayuanshu.mvpshop.ui.detail;

import com.dayuanshu.mvpshop.bean.BaseBean;
import com.dayuanshu.mvpshop.bean.GoodsDetail;


import io.reactivex.rxjava3.core.Flowable;

public interface GoodsDetailContract {

    interface IGoodsDetailPresenter{
       void getGoodsDetail(int goodsId);
    }

    interface IGoodsDetailModel{
        Flowable<BaseBean<GoodsDetail>> getGoodsDetail(int goodsId);
    }
    interface IGoodsDetailView{
        void getGoodsDetailSuccess(GoodsDetail goodsDetails);
        void getGoodsDetailError(Throwable throwable);
    }
}
