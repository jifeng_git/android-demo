package com.dayuanshu.mvpshop.network.service;

import com.dayuanshu.mvpshop.bean.BaseBean;
import com.dayuanshu.mvpshop.bean.Goods;
import com.dayuanshu.mvpshop.bean.GoodsDetail;

import java.util.List;

import io.reactivex.rxjava3.core.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoodsService {

    /**
     * 获取商品详情
     * @return
     */
    @GET("edu-lance/edu-lance.github.io/master/goods_list")
    Flowable<BaseBean<List<Goods>>> getGoods();

    /**
     * 获取商品详情
     * @param goodsId 商品编号
     * @return
     */
    @GET("edu-lance/edu-lance.github.io/master/goods_detail")
    Flowable<BaseBean<GoodsDetail>> getGoodsDetail(@Query("goodsId") int goodsId);
}
