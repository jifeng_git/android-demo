package com.dayuanshu.mvpshop.ui.detail;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.dayuanshu.mvpshop.R;
import com.dayuanshu.mvpshop.bean.GoodsDetail;
import com.dayuanshu.mvpshop.ui.base.BaseActivity;

public class GoodsDetailActivity extends BaseActivity implements View.OnClickListener, GoodsDetailContract.IGoodsDetailView {
    public final static String GOODS_ID="goods_id";
    private GoodsDetailPresenter goodsDetailPresenter;
    private Toolbar toolbar;
    private TextView detailContent;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_goods_detail;
    }

    @Override
    protected void initViews() {
        // 找到对应的控件
        toolbar = find(R.id.detail_toolBar);
        toolbar.setNavigationOnClickListener(this);
        // n内容
        detailContent=find(R.id.detail_content);
        // 请求数据
        int goodsId = getIntent().getIntExtra(GOODS_ID, 0);

        goodsDetailPresenter = new GoodsDetailPresenter(this);
        // 获取数据
        goodsDetailPresenter.getGoodsDetail(goodsId);
    }

    /**
     * 点击回退按钮的监听
     * @param v
     */
    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public void getGoodsDetailSuccess(GoodsDetail goodsDetails) {
        toolbar.setTitle(goodsDetails.getName());
        detailContent.setText(goodsDetails.getDescription());
    }

    @Override
    public void getGoodsDetailError(Throwable throwable) {
        Toast.makeText(this,"获取商品详情失败",Toast.LENGTH_SHORT).show();
    }
}
