package com.dayuanshu.networkdemo;

import androidx.annotation.NonNull;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CookieUnitTest {
    Map<String,List<Cookie>> cookies=new HashMap<>();
    @Test
    public void cookieTest() throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cookieJar(new CookieJar() {
                    @Override
                    public void saveFromResponse(@NonNull HttpUrl httpUrl, @NonNull List<Cookie> list) {
                        cookies.put(httpUrl.host(),list);
                    }

                    @NonNull
                    @Override
                    public List<Cookie> loadForRequest(@NonNull HttpUrl httpUrl) {
                        List<Cookie> cookies = CookieUnitTest.this.cookies.get(httpUrl.host());
                        return cookies==null?new ArrayList<>():cookies;
                    }
                })
                .build();

        FormBody formBody = new FormBody.Builder().add("username", "jifeng888")
                .add("password", "tian5985").build();

        Request request = new Request.Builder()
                .url("https://www.wanandroid.com/user/login")
                .post(formBody)
                .build();
        // 准备好请去的call对象
        Call call = okHttpClient.newCall(request);
        Response response = call.execute();
//        System.out.println(response.isSuccessful());
        System.out.println(response.body().string());

        // 收藏文章列表
        request=new Request.Builder().url("https://www.wanandroid.com/lg/collect/list/0/json")
                .build();
        // 准备好请求的call对象
        call = okHttpClient.newCall(request);
        try {
            Response response1 = call.execute();
            System.out.println(response1.body().string());
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
