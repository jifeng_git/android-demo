package com.dayuanshu.networkdemo;

import org.junit.Test;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AnnotationUnitTest {
    Retrofit retrofit = new Retrofit.Builder().baseUrl("https://www.httpbin.org/").build();
    IHttpbinService httpbinService=retrofit.create(IHttpbinService.class);

    @Test
    public void bodyTest() throws IOException {
        FormBody formBody = new FormBody.Builder()
                .add("a", "1")
                .add("b", "2")
                .build();
        Response<ResponseBody> execute = httpbinService.postBody(formBody).execute();
        System.out.println("测试成功:"+execute.body().string());

    }

    @Test
    public void pathTest() throws IOException {
        // https://www.httpbin.org/post
        Response<ResponseBody> execute = httpbinService.postInPath("post","android","jifeng888","tian5985").execute();
        System.out.println(execute.body().string());
    }

    @Test
    public void headersTest() throws IOException {
        // https://www.httpbin.org/post
        Call<ResponseBody> requestBodyCall = httpbinService.posWithHeaders();
        System.out.println(requestBodyCall.execute().body().string());

    }

    @Test
    public void urlTest() throws IOException {
        // https://www.httpbin.org/post
        Response<ResponseBody> response = httpbinService.posUrl("https://www.httpbin.org/post").execute();
        System.out.println(response.body().string());

    }
}
