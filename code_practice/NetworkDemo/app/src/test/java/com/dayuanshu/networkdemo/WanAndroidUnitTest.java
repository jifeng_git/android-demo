package com.dayuanshu.networkdemo;

import androidx.annotation.NonNull;

import com.dayuanshu.networkdemo.bean.BaseResponse;
import com.google.gson.Gson;

import org.junit.Test;
import org.reactivestreams.Publisher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Scheduler;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class WanAndroidUnitTest {

    Retrofit retrofit = new Retrofit.Builder().baseUrl("https://www.wanandroid.com/").build();
    IWanAndroidService wanAndroidService=retrofit.create(IWanAndroidService.class);

    @Test
    public void loginTest() throws IOException {
        Call<ResponseBody> call = wanAndroidService.login("jifeng888", "tian5985");
        Response<ResponseBody> response = call.execute();
        String result=response.body().string();
        System.out.println(result);
        // 手动进行数据转换
        BaseResponse baseResponse = new Gson().fromJson(result, BaseResponse.class);
        System.out.println(baseResponse);
    }

    // 添加转换器
    Retrofit retrofit2 = new Retrofit.Builder()
            .baseUrl("https://www.wanandroid.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    IWanAndroidService2 wanAndroidService2=retrofit2.create(IWanAndroidService2.class);
    @Test
    public void loginConvertTest() throws IOException {
        Call<BaseResponse> call = wanAndroidService2.login("jifeng888", "tian5985");

        Response<BaseResponse> response = call.execute();
        BaseResponse baseResponse = response.body();
        System.out.println(baseResponse);
    }


    /*Map<String,List<Cookie>> cookies=new HashMap<>();
    // 添加适配器
    Retrofit retrofit3 = new Retrofit.Builder()
            .baseUrl("https://www.wanandroid.com/")
            .callFactory(new OkHttpClient.Builder()
                    .cookieJar(new CookieJar() {
                        @Override
                        public void saveFromResponse(@NonNull HttpUrl httpUrl, @NonNull List<Cookie> list) {
                            cookies.put(httpUrl.host(),list);
                        }

                        @NonNull
                        @Override
                        public List<Cookie> loadForRequest(@NonNull HttpUrl httpUrl) {
                            List<Cookie> cookies = WanAndroidUnitTest.this.cookies.get(httpUrl.host());
                            return cookies==null?new ArrayList<>():cookies;
                        }
                    }).build())
            .addConverterFactory(GsonConverterFactory.create())// 添加转换器
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create()) //添加适配器
            .build();

    IWanAndroidService2 wanAndroidService3=retrofit3.create(IWanAndroidService2.class);

    @Test
    public void rxJava3Test(){
//        Call<BaseResponse> baseResponseCall = wanAndroidService3.login2("jifeng888", "tian5985");
        wanAndroidService3.login2("jifeng888", "tian5985")
                .flatMap(new Function<BaseResponse>(){
                    @Override
                    public Publisher<?> apply(BaseResponse baseResponse) throws Throwable{
                        return wanAndroidService3.getArticle(0);
                    }
                })
                .observeOn(Scheduler.io())
                .subscribeOn(Scheduler.newThread())
                .subscribe(new Consumer<ResponseBody>(){
                    @Override
                    public void accept(ResponseBody responseBody) throws Throwable{
                        System.out.println(responseBody.string());
                    }
                });
    }*/
}
