package com.dayuanshu.networkdemo;


import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UploadUnitTest {

    Retrofit retrofit=new Retrofit.Builder().baseUrl("https://www.httpbin.org/").build();
//    Retrofit retrofit=new Retrofit.Builder().build();
    UploadService uploadService=retrofit.create(UploadService.class);

    @Test
    public void uploadFileTest() throws IOException {
        File file1 = new File("E:\\logs\\1.txt");
        MultipartBody.Part part=MultipartBody.Part.createFormData("file1"
        ,"1.txt", RequestBody.create(file1, MediaType.parse("text/plain")));

        Call<ResponseBody> call = uploadService.upload(part);
        System.out.println(call.execute().body().string());
    }

    @Test
    public void downloadFileTest() throws IOException {
        Response<ResponseBody> execute = uploadService.download("https://codeload.github.com/vuejs/devtools/zip/refs/heads/main").execute();
        InputStream inputStream = execute.body().byteStream();
        FileOutputStream fos = new FileOutputStream("E:\\logs\\a.apk");
        int len;
        byte[] buff = new byte[4096];
        while ((len=inputStream.read(buff))!=-1){
            fos.write(buff,0,len);
        }
        fos.close();
        inputStream.close();
    }

   /* @Test
    public void downloadRxJavaTest() throws Exception {
        Response<ResponseBody> execute = uploadService
                .downloadRxJava("https://codeload.github.com/vuejs/devtools/zip/refs/heads/main")
                .map(new Function<ResponseBody,File>(){
                    @Override
                    public File apply(ResponseBody responseBody) {
                        InputStream inputStream = responseBody.byteStream();
                        FileOutputStream fos=null;
                        File file = new File("E:\\logs\\a.apk");
                        try {
                            fos = new FileOutputStream(file);
                            int len;
                            byte[] buff = new byte[4096];
                            while ((len=inputStream.read(buff))!=-1){
                                fos.write(buff,0,len);
                            }
                            fos.close();
                        }catch (Exception e){
                            e.getStackTrace();
                        }finally {
                            inputStream.close();
                            fos.close();
                        }
                        return file;
                    }
                }).subscribe(new Consumer<R>() {
                    @Override
                    public void accept(File file) throws Throwable {

                    }
                });
        while (true){}

    }*/

}
