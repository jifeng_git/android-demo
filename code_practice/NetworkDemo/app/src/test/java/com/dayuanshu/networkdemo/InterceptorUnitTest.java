package com.dayuanshu.networkdemo;

import androidx.annotation.NonNull;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class InterceptorUnitTest {

    @Test
    public void interceptorTest() throws IOException {
        // 拦截器可以添加很多个 addNetworkInterceptor在后面执行，addInterceptor在前面执行cache缓存
        OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .cache(new Cache(new File("E:\\logs"),1024*1024))
                .addInterceptor(new Interceptor() {
            @NonNull
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                // 前置处理
                Request request = chain.request().newBuilder().addHeader("os", "android")
                        .addHeader("version", "1.0").build();
                Response response = chain.proceed(request);
                // 后置处理
                return response;
            }
        }).addNetworkInterceptor(new Interceptor() {
            @NonNull
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                System.out.println("version:"+chain.request().header("version"));
                return chain.proceed(chain.request());
            }
        }).build();
        Request request= new Request.Builder().url("https://www.httpbin.org/get?a=1&b=2")
                .build();
        // 准备好的call对象
        Call call = okHttpClient.newCall(request);
        Response response = call.execute();
        System.out.println(response.body().string());
    }
}
