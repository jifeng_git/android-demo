package com.dayuanshu.networkdemo;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface IHttpbinService {

    @POST(value = "post")
    @FormUrlEncoded
    Call<ResponseBody> post(@Field("username") String userName, @Field("password") String pwd);
    /*@GET(value = "get")
    Call<RequestBody> get(@Query("username") String userName, @Query("password") String pwd);*/

    @GET(value = "get")
    Call<ResponseBody> get(@QueryMap Map<String,Object> map);

    @HTTP(method = "GET",path = "get",hasBody = false)
    Call<ResponseBody> http(@Query("username") String userName, @Query("password") String pwd);

    @POST(value = "post")
    Call<ResponseBody> postBody(@Body RequestBody body);

    @POST("{id}")
    @FormUrlEncoded
    Call<ResponseBody> postInPath(@Path("id") String path, @Header("os") String os,@Field("username") String userName, @Field("password") String pwd);

    @Headers({"os:android","version:1.0"})
    @POST("post")
    Call<ResponseBody> posWithHeaders();


    @POST
    Call<ResponseBody> posUrl(@Url String url);
}
