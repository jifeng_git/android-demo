package com.dayuanshu.networkdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    private OkHttpClient okHttpClient;
    private Retrofit retrofit;
    private IHttpbinService httpbinService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         okHttpClient = new OkHttpClient();
         // 创建Retrofit对象
        retrofit = new Retrofit.Builder().baseUrl("https://www.httpbin.org/").build();
        httpbinService=retrofit.create(IHttpbinService.class);
    }

    // Get同步请求
    public void getSync(View view){
        new Thread(){
            @Override
            public void run() {
                Request request = new Request.Builder().url("https://www.httpbin.org/get?a=1&b=2").build();
                // 准备好请求的call对象
                Call call = okHttpClient.newCall(request);
                try {
                    Response execute = call.execute();
                    Log.e("dys","getSync:--->"+execute.body().string());
                }catch (Exception e){
                    e.getStackTrace();
                }
            }
        }.start();
    }

    public void getASync(View view) {
        Request request = new Request.Builder().url("https://www.httpbin.org/get?a=1&b=2").build();
        // 准备好请求的call对象
        Call call = okHttpClient.newCall(request);
        // 异步请求
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {

            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if(response.isSuccessful()){
                    Log.e("dys","异步getASync:--->"+response.body().string());
                }
            }
        });
    }

    // post同步请求
    public void PostSync(View view) {
        new Thread(){
            @Override
            public void run() {
                FormBody formBody = new FormBody.Builder().add("a", "1").add("b", "2").build();
                Request request = new Request.Builder().url("https://www.httpbin.org/post").post(formBody).build();
                // 准备好请求的call对象
                Call call = okHttpClient.newCall(request);
                try {
                    Response execute = call.execute();
                    Log.e("dys","PostSync:--->"+execute.body().string());
                }catch (Exception e){
                    e.getStackTrace();
                }
            }
        }.start();
    }

    public void PostASync(View view) {
        /*FormBody formBody = new FormBody.Builder().add("a", "1").add("b", "2").build();
        Request request = new Request.Builder().url("https://www.httpbin.org/post").post(formBody).build();
        // 准备好请求的call对象
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {

            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if(response.isSuccessful()){
                    Log.e("dys","异步PostASync:--->"+response.body().string());
                }
            }
        });*/
        retrofit2.Call<ResponseBody> call = httpbinService.post("jifeng888", "tian5985");
        call.enqueue(new retrofit2.Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    Log.e("dys","异步PostASync:--->"+response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}